import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:tib_bank/config/constants/colors.dart';
import 'package:tib_bank/config/helper/text_styles.dart';

class CustomAppbar extends StatelessWidget implements PreferredSizeWidget {
// Properties

  final String title;
  final Widget leading;
  final List<Widget> actions;
  final Color bgColor;
  final bool centerTitle;
  final double elevation;
  final Color textColor;
  final Color iconColor;
  final Brightness brightness;

// Cosntructor

  CustomAppbar({
    this.title,
    this.leading,
    this.actions,
    this.bgColor,
    this.centerTitle,
    this.elevation,
    this.textColor,
    this.iconColor,
    this.brightness,
  });

  Size get preferredSize => Size.fromHeight(AppBar().preferredSize.height);

// Build
  @override
  Widget build(BuildContext context) {
    return PreferredSize(
      preferredSize: preferredSize,
      child: AppBar(
        brightness: brightness != null ? brightness : Brightness.light,
        backgroundColor: bgColor != null ? bgColor : AppColors.white,
        elevation: 0,
        flexibleSpace: ClipRect(
          child: BackdropFilter(
            filter: ImageFilter.blur(sigmaX: 7, sigmaY: 7),
            child: Container(
              color: Colors.transparent,
            ),
          ),
        ),
        centerTitle: centerTitle != null ? centerTitle : false,
        title: Text(
          title,
          style: AppText.secondaryHeading(
            color: textColor != null ? textColor : AppColors.text,
          ),
        ),
        iconTheme: IconThemeData(
          color: iconColor != null ? iconColor : AppColors.text,
        ),
        actions: actions != null ? actions : [],
      ),
    );
  }
}
