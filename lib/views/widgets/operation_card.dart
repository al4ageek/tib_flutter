import 'package:flutter/material.dart';
import 'package:ionicons/ionicons.dart';
import 'package:tib_bank/config/constants/colors.dart';
import 'package:tib_bank/config/helper/helper.dart';
import 'package:tib_bank/config/helper/text_styles.dart';

class OperationCard extends StatelessWidget {
  // Properties

  final String title;
  final IconData icon;
  final Color color;
  final Function onTap;

  // Constructor

  OperationCard({
    Key key,
    this.title,
    this.icon,
    this.color,
    this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      highlightColor: Colors.transparent,
      focusColor: Colors.transparent,
      splashColor: Colors.transparent,
      hoverColor: Colors.transparent,
      onTap: onTap,
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 5),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(5),
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(5),
              border: Border.all(color: AppColors.primary, width: 2),
            ),
            child: Stack(
              children: <Widget>[
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      width: double.infinity,
                      height: 180,
                      padding: EdgeInsets.all(20),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Icon(
                            icon,
                            size: 50,
                            color: AppColors.secondary,
                          ),
                          Padding(
                            padding: EdgeInsets.only(top: 25),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Expanded(
                                child: Wrap(
                                  children: [
                                    Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          title,
                                          style: AppText.secondaryHeading(),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                              Icon(
                                Helper.isRtl(context)
                                    ? Ionicons.arrow_back_sharp
                                    : Ionicons.arrow_forward_sharp,
                                size: 25,
                                color: AppColors.text,
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                Helper.isRtl(context)
                    ? Positioned(
                        child: Icon(
                          icon,
                          size: 80,
                          color: color.withAlpha(20),
                        ),
                        bottom: 10,
                        left: 10,
                      )
                    : Positioned(
                        child: Icon(
                          icon,
                          size: 80,
                          color: color.withAlpha(20),
                        ),
                        bottom: 10,
                        right: 10,
                      ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
