import 'package:flutter/material.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';
import 'package:tib_bank/config/constants/colors.dart';
import 'package:tib_bank/config/helper/text_styles.dart';
import 'package:tib_bank/config/helper/ui_styles.dart';

class AccountListCard extends StatelessWidget {
// Propertries

  final String title, accountNo, amount, image;
  final IconData icon;
  final Widget trailing;
  final bool isCard;
  final double cardProgress;

// Constructor

  AccountListCard({
    this.title,
    this.accountNo,
    this.amount,
    this.image,
    this.icon,
    this.trailing,
    this.isCard,
    this.cardProgress,
  });

// Build
  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.only(bottom: 10),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              width: 80,
              height: 80,
              margin: EdgeInsets.symmetric(horizontal: 15),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(5),
                image: DecorationImage(
                  image: AssetImage(
                    image,
                  ),
                ),
              ),
              child: Center(
                child: isCard != null && isCard
                    ? new CircularPercentIndicator(
                        radius: 60.0,
                        lineWidth: 5.0,
                        percent: .50,
                        circularStrokeCap: CircularStrokeCap.round,
                        center: new Text(
                          '50%',
                          style: AppText.smallTextBold(
                            color: AppColors.white,
                          ),
                        ),
                        progressColor: AppColors.white,
                      )
                    : Icon(
                        icon,
                        color: AppColors.white,
                      ),
              ),
            ),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    title,
                    style: AppText.content(),
                  ),
                  Text(
                    accountNo,
                    style: AppText.contentBold(color: AppColors.text),
                  ),
                  Text(
                    amount,
                    style: AppText.secondaryHeading(color: AppColors.primary),
                  ),
                ],
              ),
            ),
            trailing != null ? trailing : Container(),
          ],
        ));
  }
}
