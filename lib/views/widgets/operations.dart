import 'package:flutter/material.dart';
import 'package:ionicons/ionicons.dart';
import 'package:tib_bank/config/constants/colors.dart';
import 'package:tib_bank/config/helper/helper.dart';
import 'package:tib_bank/config/lang/app_locale.dart';
import 'package:tib_bank/views/pages/send_money/send_money.dart';
import 'package:tib_bank/views/widgets/custom_heading.dart';
import 'package:tib_bank/views/widgets/operation_card.dart';

class OperationsArea extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.fromLTRB(10, 0, 10, 15),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          CustomHeading(
            title: AppLocalizations.of(context).translate('operations'),
          ),
          Padding(
            padding: EdgeInsets.only(top: 15),
          ),
          Row(
            children: [
              Expanded(
                child: OperationCard(
                  color: AppColors.primary,
                  icon: Ionicons.wallet_outline,
                  title: AppLocalizations.of(context).translate('send_money'),
                  onTap: () {
                    Helper.to(SendMoney());
                  },
                ),
              ),
              Expanded(
                child: OperationCard(
                  color: AppColors.secondary,
                  icon: Ionicons.earth_outline,
                  title: AppLocalizations.of(context).translate('apply_loans'),
                ),
              ),
            ],
          ),
          Padding(
            padding: EdgeInsets.only(top: 10),
          ),
          Row(
            children: [
              Expanded(
                child: OperationCard(
                  color: AppColors.secondary,
                  icon: Ionicons.diamond_outline,
                  title:
                      AppLocalizations.of(context).translate('exchange_rates'),
                ),
              ),
              Expanded(
                child: OperationCard(
                  color: AppColors.secondary,
                  icon: Ionicons.list_outline,
                  title: AppLocalizations.of(context).translate('transactions'),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
