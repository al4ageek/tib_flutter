import 'package:flutter/material.dart';
import 'package:tib_bank/config/helper/text_styles.dart';
import 'package:tib_bank/config/helper/ui_styles.dart';

class ServiceCard extends StatelessWidget {
// Properties

  final String title, description;
  final IconData icon;
  final Color color;
  final bool reverseCircle;
  final Function onTap;

// Constructor

  ServiceCard({
    this.icon,
    this.title,
    this.description,
    this.color,
    this.reverseCircle,
    this.onTap,
  });

// Build
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        margin: EdgeInsets.fromLTRB(15, 0, 15, 15),
        height: 205,
        width: double.infinity,
        decoration: UIStyles.cardDecoration(radius: 5),
        child: Stack(
          children: [
            Container(
              padding: EdgeInsets.all(25),
              child: Column(
                mainAxisAlignment: reverseCircle
                    ? MainAxisAlignment.start
                    : MainAxisAlignment.end,
                crossAxisAlignment: reverseCircle
                    ? CrossAxisAlignment.start
                    : CrossAxisAlignment.end,
                children: [
                  Icon(
                    icon,
                    color: color,
                    size: 45,
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 10),
                  ),
                  Text(
                    title,
                    style: AppText.contentBold(),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 10),
                  ),
                  Text(
                    description,
                    style: AppText.content(),
                    textAlign: reverseCircle ? TextAlign.left : TextAlign.right,
                  ),
                ],
              ),
            ),
            reverseCircle
                ? Positioned(
                    right: -100,
                    top: -25,
                    child: Container(
                      width: 200,
                      height: 200,
                      decoration: BoxDecoration(
                        color: color.withAlpha(20),
                        shape: BoxShape.circle,
                      ),
                    ),
                  )
                : Positioned(
                    left: -100,
                    top: -25,
                    child: Container(
                      width: 200,
                      height: 200,
                      decoration: BoxDecoration(
                        color: color.withAlpha(20),
                        shape: BoxShape.circle,
                      ),
                    ),
                  ),
            reverseCircle
                ? Positioned(
                    right: -25,
                    bottom: -100,
                    child: Container(
                      width: 200,
                      height: 200,
                      decoration: BoxDecoration(
                        color: color.withAlpha(20),
                        shape: BoxShape.circle,
                      ),
                    ),
                  )
                : Positioned(
                    left: -25,
                    bottom: -100,
                    child: Container(
                      width: 200,
                      height: 200,
                      decoration: BoxDecoration(
                        color: color.withAlpha(20),
                        shape: BoxShape.circle,
                      ),
                    ),
                  ),
          ],
        ),
      ),
    );
  }
}
