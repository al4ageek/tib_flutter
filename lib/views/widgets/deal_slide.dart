import 'package:flutter/material.dart';

class DealSlide extends StatelessWidget {
// Properties

  final String image;

// Cosntructor

  DealSlide({this.image});

//   Build

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(4.0),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(10),
        child: Image.asset(
          'assets/img/deal.png',
          fit: BoxFit.fitHeight,
        ),
      ),
    );
  }
}
