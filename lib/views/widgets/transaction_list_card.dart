import 'package:flutter/material.dart';
import 'package:tib_bank/config/constants/colors.dart';
import 'package:tib_bank/config/helper/text_styles.dart';

class TransactionListCard extends StatelessWidget {
// Properties

  final String transaction, description, amount, status;
  final bool isCredit;
  final IconData icon;

// Cosntructor

  TransactionListCard({
    this.transaction,
    this.description,
    this.amount,
    this.status,
    this.isCredit,
    this.icon,
  });

// Build

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 15),
      child: Column(
        children: [
          ListTile(
            leading: Container(
              width: 80,
              height: 80,
              decoration: BoxDecoration(
                  color: AppColors.primary.withAlpha(50),
                  borderRadius: BorderRadius.circular(5)),
              child: Center(
                child: Icon(
                  icon,
                  color: AppColors.primary,
                ),
              ),
            ),
            title: Row(
              children: [
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        transaction,
                        style: AppText.content(color: AppColors.primary),
                      ),
                      Text(
                        description,
                        style: AppText.content(),
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                      )
                    ],
                  ),
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Text(
                      amount,
                      style: AppText.content(
                        color: isCredit ? AppColors.success : AppColors.danger,
                      ),
                    ),
                    Text(
                      status,
                      style: AppText.content(),
                    )
                  ],
                ),
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 15),
          ),
          Divider(
            height: 2,
            color: AppColors.text.withAlpha(40),
          ),
        ],
      ),
    );
  }
}
