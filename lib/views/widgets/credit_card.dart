import 'package:flutter/material.dart';
import 'package:line_icons/line_icon.dart';
import 'package:tib_bank/config/constants/colors.dart';
import 'package:tib_bank/config/helper/text_styles.dart';

class CreditCard extends StatelessWidget {
// Properties

  final Color color;

// Cosntructor

  CreditCard({this.color});

// Build

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          height: 200,
          margin: EdgeInsets.fromLTRB(15, 0, 15, 15),
          decoration: BoxDecoration(
            color: color,
            borderRadius: BorderRadius.circular(5),
          ),
          child: Stack(
            children: [
              Container(
                padding: EdgeInsets.all(15),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        LineIcon.mastercardCreditCard(
                          color: AppColors.white,
                        ),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.end,
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            Text(
                              'Current Account',
                              style: AppText.content(
                                color: AppColors.white,
                              ),
                            ),
                            Text(
                              '1243 **** **** 4422',
                              style: AppText.secondaryHeading(
                                color: AppColors.white,
                              ),
                            ),
                          ],
                        )
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'Balance',
                              style: AppText.content(
                                color: AppColors.white,
                              ),
                            ),
                            Text(
                              '130,000 IQD',
                              style: AppText.secondaryHeading(
                                color: AppColors.white,
                              ),
                            ),
                          ],
                        ),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.end,
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            Text(
                              'Valid Through',
                              style: AppText.content(
                                color: AppColors.white,
                              ),
                            ),
                            Text(
                              '06/23',
                              style: AppText.secondaryHeading(
                                color: AppColors.white,
                              ),
                            ),
                          ],
                        )
                      ],
                    ),
                  ],
                ),
              ),
              Positioned(
                left: -100,
                top: -25,
                child: Container(
                  width: 150,
                  height: 150,
                  decoration: BoxDecoration(
                    color: AppColors.white.withAlpha(30),
                    shape: BoxShape.circle,
                  ),
                ),
              ),
              Positioned(
                left: -100,
                bottom: -25,
                child: Container(
                  width: 180,
                  height: 180,
                  decoration: BoxDecoration(
                    color: AppColors.white.withAlpha(30),
                    shape: BoxShape.circle,
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
