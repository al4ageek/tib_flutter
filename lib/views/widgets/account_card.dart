import 'package:flutter/material.dart';
import 'package:tib_bank/config/helper/text_styles.dart';
import 'package:tib_bank/config/constants/colors.dart';
import 'package:tib_bank/config/helper/ui_styles.dart';

class AccountCard extends StatelessWidget {
// Properties

  final String accoutNo, expiry, address, amount, type;
  final Function onTap;

// Cosntructro

  AccountCard({
    @required this.accoutNo,
    @required this.expiry,
    @required this.address,
    @required this.amount,
    @required this.type,
    @required this.onTap,
  });

// Build

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        padding: EdgeInsets.all(25),
        margin: EdgeInsets.only(bottom: 10),
        decoration: UIStyles.cardDecoration(radius: 5),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        accoutNo,
                        style: AppText.contentBold(),
                      ),
                      Text(
                        address,
                        style: AppText.content(),
                      ),
                    ],
                  ),
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Text(
                      'Card Expiry',
                      style: AppText.content(),
                    ),
                    Text(
                      expiry,
                      style: AppText.contentBold(),
                    ),
                  ],
                ),
                Padding(
                  padding: EdgeInsets.only(bottom: 15),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  amount,
                  style: AppText.primaryHeading(color: AppColors.primary),
                ),
                Text(
                  type,
                  style: AppText.content(
                    color: AppColors.primary,
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
