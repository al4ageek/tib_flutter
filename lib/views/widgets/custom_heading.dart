import 'package:flutter/material.dart';
import 'package:tib_bank/config/constants/colors.dart';
import 'package:tib_bank/config/helper/helper.dart';
import 'package:tib_bank/config/helper/text_styles.dart';

class CustomHeading extends StatelessWidget {
  final String title;
  final Widget trailing;

  const CustomHeading({
    Key key,
    @required this.title,
    this.trailing,
  }) : super(key: key);

  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(15),
      width: MediaQuery.of(context).size.width,
      child: Stack(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Padding(
                padding: Helper.isRtl(context)
                    ? EdgeInsets.only(right: 40)
                    : EdgeInsets.only(left: 40),
                child: Text(
                  title,
                  style: AppText.secondaryHeading(),
                ),
              ),
              trailing != null ? trailing : Container(),
            ],
          ),
          Helper.isRtl(context)
              ? Positioned(
                  right: 0,
                  top: trailing != null ? 22 : 15,
                  child: Container(
                    width: 25,
                    height: 5,
                    decoration: BoxDecoration(
                      color: AppColors.primary,
                      borderRadius: BorderRadius.circular(50),
                    ),
                  ),
                )
              : Positioned(
                  left: 0,
                  top: trailing != null ? 22 : 8,
                  child: Container(
                    width: 25,
                    height: 5,
                    decoration: BoxDecoration(
                      color: AppColors.primary,
                      borderRadius: BorderRadius.circular(50),
                    ),
                  ),
                ),
        ],
      ),
    );
  }
}
