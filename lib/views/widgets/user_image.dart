import 'package:flutter/material.dart';
import 'package:tib_bank/config/constants/images.dart';

class UserImage extends StatelessWidget {
// Properteis

  final String image;

// Constructor

  UserImage({this.image});

// Build
  @override
  Widget build(BuildContext context) {
    return Container(
      child: ClipRRect(
        borderRadius: BorderRadius.circular(200),
        child: FadeInImage.assetNetwork(
          image: image,
          fadeInCurve: Curves.easeIn,
          placeholderScale: 50.50,
          fadeInDuration: Duration(milliseconds: 300),
          repeat: ImageRepeat.noRepeat,
          imageSemanticLabel: 'Image',
          fit: BoxFit.cover,
          placeholder: AppImages.logo,
          width: 30,
          height: 30,
        ),
      ),
    );
  }
}
