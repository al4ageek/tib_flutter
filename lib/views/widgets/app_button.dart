import 'package:flutter/material.dart';
import 'package:tib_bank/config/helper/text_styles.dart';

class CustomButton extends StatelessWidget {
// Properties

  final String title;
  final IconData icon;
  final Function onTap;
  final double radius;
  final Color backgroundColor, textColor;
  final bool hasBorder;
  final Color borderColor;
  final double width;

// Constructor

  CustomButton({
    this.title,
    this.icon,
    this.onTap,
    this.radius,
    this.backgroundColor,
    this.textColor,
    this.hasBorder,
    this.borderColor,
    this.width,
  });

// Build
  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      color: backgroundColor,
      elevation: 0,
      height: 50,
      minWidth: width != null ? width : 50,
      child: Row(
        mainAxisAlignment: icon != null || title != null
            ? MainAxisAlignment.spaceBetween
            : MainAxisAlignment.center,
        children: [
          title != null
              ? Text(
                  title,
                  style: AppText.buttonText(color: textColor),
                  textAlign:
                      icon == null ? TextAlign.center : TextAlign.justify,
                )
              : Container(),
          Icon(
            icon,
            color: textColor,
          )
        ],
      ),
      onPressed: onTap,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(radius),
        side: BorderSide(
          color: borderColor != null ? borderColor : Colors.transparent,
          width: 1,
          style: BorderStyle.solid,
        ),
      ),
    );
  }
}
