import 'package:flutter/material.dart';
import 'package:tib_bank/config/constants/colors.dart';
import 'package:tib_bank/config/helper/text_styles.dart';

class TimeSlot extends StatelessWidget {
// Properties

  final String time;
  final bool isDisabled, isSelected;

// Constructor

  TimeSlot({this.time, this.isDisabled, this.isSelected});

// Build
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50,
      width: 110,
      margin: EdgeInsets.only(right: 15, bottom: 15),
      decoration: BoxDecoration(
        color: isDisabled
            ? AppColors.background
            : isSelected
                ? AppColors.white
                : AppColors.lightGreen,
        borderRadius: BorderRadius.circular(5),
        border: Border.all(
          color: isSelected ? AppColors.secondary : AppColors.white,
        ),
      ),
      child: Center(
        child: Text(
          time,
          style: AppText.content(
              color: isSelected ? AppColors.secondary : AppColors.text),
        ),
      ),
    );
  }
}
