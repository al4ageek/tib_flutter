import 'package:flutter/material.dart';
import 'package:ionicons/ionicons.dart';
import 'package:tib_bank/config/constants/colors.dart';
import 'package:tib_bank/config/helper/helper.dart';
import 'package:tib_bank/config/helper/text_styles.dart';

class MenuListItem extends StatelessWidget {
// Props

  final String title;
  final IconData icon;
  final Function onTap;

// Constructor

  MenuListItem({this.title, this.icon, this.onTap});

// Build

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          ListTile(
            onTap: onTap,
            leading: Icon(
              icon,
              color: AppColors.primary,
            ),
            title: Text(
              title,
              style: AppText.content(),
            ),
            trailing: Icon(
              Helper.isRtl(context)
                  ? Ionicons.chevron_back
                  : Ionicons.chevron_forward,
              color: AppColors.text,
              size: 15,
            ),
          ),
          Divider(
            height: 25,
            color: AppColors.text.withAlpha(40),
          ),
        ],
      ),
    );
  }
}
