import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:tib_bank/config/helper/text_styles.dart';

class EmptyArea extends StatelessWidget {
  final String title, message;
  const EmptyArea({Key key, this.message, this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(50),
      child: Column(
        children: <Widget>[
          Image.asset(
            'assets/img/empty.png',
            width: 300,
          ),
          Padding(
            padding: EdgeInsets.only(top: 50),
          ),
          Text(
            this.title,
            style: AppText.contentBold(),
          ),
          Padding(
            padding: EdgeInsets.only(top: 15),
          ),
          Text(
            this.message,
            style: AppText.content(),
            textAlign: TextAlign.center,
          )
        ],
      ),
    );
  }
}
