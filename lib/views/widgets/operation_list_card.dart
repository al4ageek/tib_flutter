import 'package:flutter/material.dart';
import 'package:ionicons/ionicons.dart';
import 'package:tib_bank/config/constants/colors.dart';
import 'package:tib_bank/config/helper/text_styles.dart';
import 'package:tib_bank/config/helper/ui_styles.dart';

class OperationListCard extends StatelessWidget {
// Properties

  final String title;
  final IconData icon;
  final Color color;
  final Function onTap;

// Constructor

  OperationListCard({
    this.icon,
    this.title,
    this.color,
    this.onTap,
  });

// Build
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      highlightColor: Colors.transparent,
      focusColor: Colors.transparent,
      hoverColor: Colors.transparent,
      splashColor: Colors.transparent,
      child: Container(
        margin: EdgeInsets.fromLTRB(0, 0, 0, 10),
        height: 155,
        width: double.infinity,
        decoration: UIStyles.cardDecoration(radius: 5),
        child: Stack(
          children: [
            Container(
              padding: EdgeInsets.all(25),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Row(
                    children: [
                      Expanded(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Icon(
                              icon,
                              color: color,
                              size: 30,
                            ),
                            Padding(
                              padding: EdgeInsets.only(top: 10),
                            ),
                            Text(
                              title,
                              style: AppText.contentBold(),
                            ),
                          ],
                        ),
                      ),
                      Icon(
                        Ionicons.arrow_forward,
                        color: AppColors.primary,
                      )
                    ],
                  ),
                ],
              ),
            ),
            Positioned(
              right: -100,
              top: -25,
              child: Container(
                width: 200,
                height: 200,
                decoration: BoxDecoration(
                  color: color.withAlpha(10),
                  shape: BoxShape.circle,
                ),
              ),
            ),
            Positioned(
              right: -25,
              bottom: -100,
              child: Container(
                width: 200,
                height: 200,
                decoration: BoxDecoration(
                  color: color.withAlpha(10),
                  shape: BoxShape.circle,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
