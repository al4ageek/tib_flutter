import 'package:flutter/material.dart';

class MenuIcon extends StatelessWidget {
// Properties

  final IconData icon;
  final Function onTap;
  final Color color;

// Constructor

  MenuIcon({this.icon, this.onTap, this.color});

// Build
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      enableFeedback: false,
      highlightColor: Colors.transparent,
      focusColor: Colors.transparent,
      splashColor: Colors.transparent,
      child: Container(
        width: 75,
        height: 80,
        child: Center(
          child: Icon(
            icon,
            color: color,
            size: 25,
          ),
        ),
      ),
    );
  }
}
