import 'package:flutter/material.dart';
import 'package:tib_bank/config/helper/text_styles.dart';
import 'package:tib_bank/config/helper/ui_styles.dart';
import 'package:tib_bank/views/widgets/user_image.dart';

class PayeeCard extends StatelessWidget {
// Properteis

  final String image, username, bank;

// Constructor

  PayeeCard({this.image, this.username, this.bank});

//

  @override
  Widget build(BuildContext context) {
    return Container(
        width: 200,
        height: 220,
        decoration: UIStyles.cardDecoration(radius: 10),
        margin: EdgeInsets.only(
          bottom: 10,
        ),
        padding: EdgeInsets.all(15),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              width: 80,
              height: 80,
              child: UserImage(
                image: image,
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 15),
            ),
            Text(
              username,
              style: AppText.contentBold(),
            ),
            Text(
              bank,
              style: AppText.content(),
            ),
          ],
        ));
  }
}
