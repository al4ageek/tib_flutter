import 'package:flutter/material.dart';
import 'package:ionicons/ionicons.dart';
import 'package:tib_bank/config/lang/app_locale.dart';
import 'package:tib_bank/views/widgets/app_bar.dart';
import 'package:tib_bank/views/widgets/list_item.dart';

class Settings extends StatefulWidget {
  @override
  _SettingsState createState() => _SettingsState();
}

class _SettingsState extends State<Settings> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppbar(
        title: AppLocalizations.of(context).translate('settings'),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            MenuListItem(
              title: AppLocalizations.of(context).translate('change_password'),
              icon: Ionicons.lock_closed_outline,
            ),
            MenuListItem(
              title: AppLocalizations.of(context).translate('face_id'),
              icon: Ionicons.happy_outline,
            ),
            MenuListItem(
              title: AppLocalizations.of(context).translate('terms_conditions'),
              icon: Ionicons.document_outline,
            ),
          ],
        ),
      ),
    );
  }
}
