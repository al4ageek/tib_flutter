import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ionicons/ionicons.dart';
import 'package:tib_bank/config/constants/colors.dart';
import 'package:tib_bank/config/lang/app_locale.dart';
import 'package:tib_bank/controller/stepper_ctrl.dart';
import 'package:tib_bank/views/pages/schedule_call/schedule_date.dart';
import 'package:tib_bank/views/pages/schedule_call/schedule_info.dart';
import 'package:tib_bank/views/pages/schedule_call/schedule_steps.dart';
import 'package:tib_bank/views/pages/schedule_call/schedule_success.dart';
import 'package:tib_bank/views/widgets/app_bar.dart';
import 'package:tib_bank/views/widgets/app_button.dart';

class ScheduleCall extends StatefulWidget {
  @override
  _ScheduleCallState createState() => _ScheduleCallState();
}

class _ScheduleCallState extends State<ScheduleCall> {
// Properties

  List<Widget> pages = [
    ScheduleInfo(),
    ScheduleDate(),
    ScheduleSuccess(),
  ];
  StepperController stepCtrl = Get.put(StepperController());

// Initstate

  @override
  void initState() {
    super.initState();
    stepCtrl.pages = pages;
  }

// Build

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.primary,
      appBar: CustomAppbar(
        title: AppLocalizations.of(context).translate('schedule'),
        bgColor: Colors.transparent,
        elevation: 0,
        textColor: AppColors.white,
        centerTitle: false,
        iconColor: AppColors.white,
        brightness: Brightness.dark,
      ),
      body: Stack(
        children: [
          Positioned(
            left: 15,
            top: 12,
            child: Container(
              width: MediaQuery.of(context).size.width - 30,
              height: MediaQuery.of(context).size.height,
              decoration: BoxDecoration(
                color: Colors.white.withAlpha(80),
                borderRadius: BorderRadius.circular(10),
              ),
            ),
          ),
          Positioned(
            left: 30,
            top: 0,
            child: Container(
              width: MediaQuery.of(context).size.width - 60,
              height: MediaQuery.of(context).size.height - 140,
              decoration: BoxDecoration(
                color: Colors.white.withAlpha(80),
                borderRadius: BorderRadius.circular(10),
              ),
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(10),
            ),
            child: Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              padding: EdgeInsets.all(15),
              child: SingleChildScrollView(
                child: ScheduleSteps(),
              ),
            ),
            margin: EdgeInsets.only(top: 25),
          ),
          Positioned(
            bottom: 0,
            left: 0,
            child: SafeArea(
              child: Container(
                width: MediaQuery.of(context).size.width,
                padding: EdgeInsets.symmetric(horizontal: 15),
                child: Obx(
                  () => CustomButton(
                    title: stepCtrl.stepIndex.value != 3
                        ? AppLocalizations.of(context).translate('next')
                        : AppLocalizations.of(context).translate('finish'),
                    // icon: Ionicons.arrow_forward,
                    backgroundColor: AppColors.primary,
                    textColor: Colors.white,
                    radius: 5,
                    icon: Ionicons.arrow_forward,
                    onTap: () {
                      if (stepCtrl.stepIndex.value < 2) {
                        stepCtrl.nextPage();
                      } else {}
                    },
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
