import 'package:calendar_timeline/calendar_timeline.dart';
import 'package:date_picker_timeline/date_picker_widget.dart';
import 'package:flutter/material.dart';
import 'package:tib_bank/config/constants/colors.dart';
import 'package:tib_bank/config/helper/text_styles.dart';
import 'package:tib_bank/config/lang/app_locale.dart';
import 'package:tib_bank/views/widgets/time_slot.dart';
// import 'package:ms_date_picker_timeline/ms_date_picker_timeline.dart';

class ScheduleDate extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.symmetric(vertical: 25),
            child: Text(
              AppLocalizations.of(context)
                  .translate('select_slot')
                  .toUpperCase(),
              style: AppText.contentBold(),
            ),
          ),
          DatePicker(
            DateTime.now(),
            initialSelectedDate: DateTime.now(),
            selectionColor: AppColors.secondary,
            selectedTextColor: Colors.white,
            dateTextStyle: AppText.content(color: AppColors.primary),
            onDateChange: (date) {},
          ),
          IntrinsicHeight(
            child: Container(
              padding: EdgeInsets.symmetric(vertical: 15),
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Expanded(
                      child: Wrap(
                        children: [
                          TimeSlot(
                            time: '01:30',
                            isSelected: false,
                            isDisabled: true,
                          ),
                          TimeSlot(
                            time: '01:30',
                            isSelected: false,
                            isDisabled: true,
                          ),
                          TimeSlot(
                            time: '01:30',
                            isSelected: false,
                            isDisabled: true,
                          ),
                          TimeSlot(
                            time: '01:30',
                            isSelected: true,
                            isDisabled: false,
                          ),
                          TimeSlot(
                            time: '01:30',
                            isSelected: false,
                            isDisabled: false,
                          ),
                          TimeSlot(
                            time: '01:30',
                            isSelected: false,
                            isDisabled: false,
                          ),
                          TimeSlot(
                            time: '01:30',
                            isSelected: false,
                            isDisabled: false,
                          ),
                          TimeSlot(
                            time: '01:30',
                            isSelected: false,
                            isDisabled: false,
                          ),
                        ],
                      ),
                    ),
                  ]),
            ),
          ),
        ],
      ),
    );
  }
}
