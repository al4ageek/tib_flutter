import 'package:flutter/material.dart';
import 'package:tib_bank/config/constants/colors.dart';
import 'package:tib_bank/config/helper/text_styles.dart';
import 'package:tib_bank/config/lang/app_locale.dart';

class ScheduleSuccess extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Padding(
            padding: EdgeInsets.symmetric(vertical: 25),
            child: Text(
              AppLocalizations.of(context)
                  .translate('enter_otp_sent')
                  .toUpperCase(),
              style: AppText.contentBold(),
            ),
          ),
          Container(
            width: 220,
            margin: EdgeInsets.symmetric(vertical: 25),
            child: Image.asset(
              'assets/img/calendar.png',
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(
              vertical: 30,
            ),
            child: Text(
              'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been ..',
              style: AppText.content(),
              textAlign: TextAlign.center,
            ),
          ),
          MaterialButton(
            onPressed: () {},
            child: Text(
              'Add to calendar'.toUpperCase(),
              style: AppText.buttonText(color: AppColors.primary),
            ),
          )
        ],
      ),
    );
  }
}
