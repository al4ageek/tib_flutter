import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/src/rx_flutter/rx_obx_widget.dart';
import 'package:im_stepper/stepper.dart';
import 'package:ionicons/ionicons.dart';
import 'package:tib_bank/config/constants/colors.dart';
import 'package:tib_bank/controller/stepper_ctrl.dart';

class ScheduleSteps extends StatelessWidget {
// Properites

  final StepperController stepCtrl = Get.find();

// Build

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          width: MediaQuery.of(context).size.width,
          child: Center(
            child: Obx(
              () => IconStepper(
                activeStep: stepCtrl.stepIndex.value,
                activeStepColor: AppColors.secondary,
                activeStepBorderColor: AppColors.secondary,
                lineColor: AppColors.secondary,
                stepColor: AppColors.primary,
                nextButtonIcon: Icon(
                  Ionicons.chevron_forward,
                  color: AppColors.primary,
                  size: 15,
                ),
                previousButtonIcon: Icon(
                  Ionicons.chevron_back,
                  color: AppColors.primary,
                  size: 15,
                ),
                onStepReached: (index) {
                  stepCtrl.stepIndex.value = index;
                  print(stepCtrl.stepIndex.value);
                },
                icons: [
                  Icon(
                    Ionicons.document_outline,
                    color: Colors.white,
                  ),
                  Icon(
                    Ionicons.calendar_outline,
                    color: Colors.white,
                  ),
                  Icon(
                    Ionicons.checkmark_done_outline,
                    color: Colors.white,
                  ),
                ],
              ),
            ),
          ),
        ),
        Obx(
          () => Column(
            children: [
              stepCtrl.getActivePage(),
            ],
          ),
        ),
      ],
    );
  }
}
