import 'package:flutter/material.dart';
import 'package:tib_bank/config/constants/colors.dart';
import 'package:tib_bank/config/helper/text_styles.dart';
import 'package:tib_bank/views/widgets/app_bar.dart';
import 'package:tib_bank/views/widgets/custom_heading.dart';

class AddAccount extends StatefulWidget {
  @override
  _AddAccountState createState() => _AddAccountState();
}

class _AddAccountState extends State<AddAccount> {
  int accountType;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppbar(
        title: 'Add Account',
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.all(25),
          child: Column(
            children: [
              CustomHeading(
                title: 'Account Information',
              ),
              TextFormField(
                decoration: InputDecoration(
                  hintText: 'Enter Account Title',
                  labelText: 'Account Title',
                  hintStyle: AppText.content(),
                  labelStyle: AppText.contentBold(),
                  alignLabelWithHint: true,
                  contentPadding: EdgeInsets.symmetric(
                    vertical: 15,
                  ),
                ),
                keyboardType: TextInputType.text,
              ),
              Padding(
                padding: EdgeInsets.only(top: 10),
              ),
              TextFormField(
                decoration: InputDecoration(
                  hintText: 'Enter Account Number',
                  labelText: 'Account Number',
                  labelStyle: AppText.contentBold(),
                  hintStyle: AppText.content(),
                  alignLabelWithHint: true,
                  contentPadding: EdgeInsets.symmetric(
                    vertical: 15,
                  ),
                ),
                keyboardType: TextInputType.number,
              ),
              Padding(
                padding: EdgeInsets.only(top: 10),
              ),
              TextFormField(
                decoration: InputDecoration(
                  hintText: 'Enter IBAN',
                  labelText: 'Account IBAN',
                  labelStyle: AppText.contentBold(),
                  hintStyle: AppText.content(),
                  alignLabelWithHint: true,
                  contentPadding: EdgeInsets.symmetric(
                    vertical: 15,
                  ),
                ),
                keyboardType: TextInputType.number,
              ),
              Padding(
                padding: EdgeInsets.only(top: 30),
              ),
              CustomHeading(
                title: 'Account Type',
              ),
              RadioListTile(
                value: 0,
                // contentPadding: EdgeInsets.all(0),
                groupValue: accountType,
                onChanged: (e) {
                  setState(() {
                    accountType = e;
                  });
                },
                title: Text(
                  'Saving Account',
                  style: AppText.content(),
                ),
              ),
              RadioListTile(
                value: 1,
                // contentPadding: EdgeInsets.all(0),
                groupValue: accountType,
                onChanged: (e) {
                  setState(() {
                    accountType = e;
                  });
                },
                title: Text(
                  'Current Account',
                  style: AppText.content(),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 30),
              ),
              MaterialButton(
                onPressed: () {},
                minWidth: double.infinity,
                child: Text(
                  'Find Account'.toUpperCase(),
                  style: AppText.buttonText(color: AppColors.white),
                ),
                color: AppColors.primary,
                height: 60,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
