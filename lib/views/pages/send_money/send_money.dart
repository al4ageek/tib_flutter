import 'package:flutter/material.dart';
import 'package:ionicons/ionicons.dart';
import 'package:tib_bank/config/constants/colors.dart';
import 'package:tib_bank/config/helper/helper.dart';
import 'package:tib_bank/config/lang/app_locale.dart';
import 'package:tib_bank/views/widgets/app_bar.dart';
import 'package:tib_bank/views/widgets/custom_heading.dart';
import 'package:tib_bank/views/widgets/operation_card.dart';
import 'package:tib_bank/views/widgets/operation_list_card.dart';

class SendMoney extends StatefulWidget {
  @override
  _SendMoneyState createState() => _SendMoneyState();
}

class _SendMoneyState extends State<SendMoney> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppbar(
        title: AppLocalizations.of(context).translate('send_money'),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.fromLTRB(10, 0, 10, 15),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: EdgeInsets.only(top: 15),
                  ),
                  Column(
                    children: [
                      OperationListCard(
                        color: AppColors.primary,
                        icon: Ionicons.wallet_outline,
                        title: 'Cash Transfer',
                        onTap: () {},
                      ),
                      OperationListCard(
                        color: AppColors.primary,
                        icon: Ionicons.earth_outline,
                        title: AppLocalizations.of(context)
                            .translate('western_union'),
                      ),
                      OperationListCard(
                        color: AppColors.primary,
                        icon: Ionicons.reload_outline,
                        title: 'Between My Accounts',
                      ),
                      OperationListCard(
                        color: AppColors.primary,
                        icon: Ionicons.color_filter_outline,
                        title: 'Other Account in Same Branch',
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
