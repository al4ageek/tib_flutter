import 'package:flutter/material.dart';
import 'package:ionicons/ionicons.dart';
import 'package:tib_bank/config/constants/colors.dart';
import 'package:tib_bank/config/helper/text_styles.dart';
import 'package:tib_bank/config/helper/ui_styles.dart';
import 'package:tib_bank/views/widgets/app_bar.dart';
import 'package:tib_bank/views/widgets/operations.dart';

class RatesResult extends StatefulWidget {
  @override
  _RatesResultState createState() => _RatesResultState();
}

class _RatesResultState extends State<RatesResult> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppbar(
        title: 'Conversion Results',
      ),
      body: SingleChildScrollView(
        child: Container(
          child: Column(
            children: [
              Container(
                padding: EdgeInsets.all(15),
                child: Column(
                  children: [
                    Container(
                      decoration: UIStyles.cardDecoration(radius: 5),
                      margin: EdgeInsets.only(bottom: 15),
                      padding: EdgeInsets.all(15),
                      child: Column(
                        children: [
                          Row(
                            children: [
                              Icon(
                                Ionicons.globe_outline,
                                color: AppColors.primary,
                              ),
                              Padding(
                                padding: EdgeInsets.symmetric(horizontal: 10),
                              ),
                              Expanded(
                                child: Text(
                                  'Westerns Union',
                                  style: AppText.content(),
                                ),
                              ),
                              Text(
                                '45000 IQD',
                                style: AppText.contentBold(
                                    color: AppColors.secondary),
                              ),
                            ],
                          ),
                          Padding(
                            padding: EdgeInsets.only(top: 15),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                'Total Amount',
                                style: AppText.content(),
                              ),
                              Text(
                                '44500 IQD',
                                style: AppText.contentBold(
                                    color: AppColors.primary),
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                'Processing Fee',
                                style: AppText.content(),
                              ),
                              Text(
                                '3000 IQD',
                                style: AppText.contentBold(
                                    color: AppColors.primary),
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                'Exchange Rate',
                                style: AppText.content(),
                              ),
                              Text(
                                '1 USD - 3000 IQD',
                                style: AppText.content(),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                    Container(
                      decoration: UIStyles.cardDecoration(radius: 5),
                      padding: EdgeInsets.all(15),
                      child: Column(
                        children: [
                          Row(
                            children: [
                              Icon(
                                Ionicons.basket_outline,
                                color: AppColors.primary,
                              ),
                              Padding(
                                padding: EdgeInsets.symmetric(horizontal: 10),
                              ),
                              Expanded(
                                child: Text(
                                  'AL TAIF BANK',
                                  style: AppText.content(),
                                ),
                              ),
                              Text(
                                '45000 IQD',
                                style: AppText.contentBold(
                                    color: AppColors.secondary),
                              ),
                            ],
                          ),
                          Padding(
                            padding: EdgeInsets.only(top: 15),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                'Total Amount',
                                style: AppText.content(),
                              ),
                              Text(
                                '45000 IQD',
                                style: AppText.contentBold(
                                    color: AppColors.primary),
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                'Processing Fee',
                                style: AppText.content(),
                              ),
                              Text(
                                '100 IQD',
                                style: AppText.contentBold(
                                    color: AppColors.primary),
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                'Exchange Rate',
                                style: AppText.content(),
                              ),
                              Text(
                                '1 USD - 3000 IQD',
                                style: AppText.content(),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              OperationsArea(),
            ],
          ),
        ),
      ),
    );
  }
}
