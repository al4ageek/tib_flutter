import 'package:flutter/material.dart';
import 'package:ionicons/ionicons.dart';
import 'package:tib_bank/config/constants/colors.dart';
import 'package:tib_bank/config/helper/helper.dart';
import 'package:tib_bank/config/helper/text_styles.dart';
import 'package:tib_bank/config/lang/app_locale.dart';
import 'package:tib_bank/views/pages/send_money/send_money.dart';
import 'package:tib_bank/views/widgets/app_bar.dart';
import 'package:tib_bank/views/widgets/custom_heading.dart';
import 'package:tib_bank/views/widgets/list_item.dart';
import 'package:tib_bank/views/widgets/operation_card.dart';
import 'package:tib_bank/views/widgets/user_image.dart';

class Profile extends StatefulWidget {
  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppbar(
        title: AppLocalizations.of(context).translate('profile'),
        actions: [
          MaterialButton(
            child: Text(
              AppLocalizations.of(context).translate('edit').toUpperCase(),
              style: AppText.buttonText(),
            ),
            onPressed: () {},
          ),
        ],
      ),
      body: Stack(
        children: [
          Positioned(
            top: -150,
            left: 0,
            child: Container(
              width: MediaQuery.of(context).size.width,
              height: 300,
              child: Container(
                width: 300,
                height: 300,
                decoration: BoxDecoration(
                  color: AppColors.secondary.withAlpha(30),
                  shape: BoxShape.circle,
                ),
              ),
            ),
          ),
          SingleChildScrollView(
            child: Container(
              width: double.infinity,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    margin: EdgeInsets.only(top: 70),
                    width: 120,
                    height: 120,
                    child: UserImage(
                      image: 'https://i.pravatar.cc/100',
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 25),
                  ),
                  Text(
                    'John Doe',
                    style: AppText.secondaryHeading(
                      color: AppColors.primary,
                    ),
                  ),
                  Text(
                    'john@gmail.com',
                    style: AppText.content(),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 40),
                  ),
                  MenuListItem(
                    title:
                        AppLocalizations.of(context).translate('my_accounts'),
                    icon: Ionicons.wallet_outline,
                    onTap: () {},
                  ),
                  MenuListItem(
                    title:
                        AppLocalizations.of(context).translate('my_requests'),
                    icon: Ionicons.document_attach_outline,
                    onTap: () {},
                  ),
                  Container(
                    padding: EdgeInsets.fromLTRB(10, 0, 10, 15),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        CustomHeading(
                          title: AppLocalizations.of(context)
                              .translate('operations'),
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 15),
                        ),
                        Row(
                          children: [
                            Expanded(
                              child: OperationCard(
                                color: AppColors.primary,
                                icon: Ionicons.wallet_outline,
                                title: AppLocalizations.of(context)
                                    .translate('send_money'),
                                onTap: () {
                                  Helper.to(SendMoney());
                                },
                              ),
                            ),
                            Expanded(
                              child: OperationCard(
                                color: AppColors.secondary,
                                icon: Ionicons.earth_outline,
                                title: AppLocalizations.of(context)
                                    .translate('apply_loans'),
                              ),
                            ),
                          ],
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 10),
                        ),
                        Row(
                          children: [
                            Expanded(
                              child: OperationCard(
                                color: AppColors.secondary,
                                icon: Ionicons.diamond_outline,
                                title: AppLocalizations.of(context)
                                    .translate('exchange_rates'),
                              ),
                            ),
                            Expanded(
                              child: OperationCard(
                                color: AppColors.secondary,
                                icon: Ionicons.list_outline,
                                title: AppLocalizations.of(context)
                                    .translate('transactions'),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
