import 'package:flutter/material.dart';
import 'package:ionicons/ionicons.dart';
import 'package:tib_bank/config/constants/colors.dart';
import 'package:tib_bank/views/widgets/custom_heading.dart';
import 'package:tib_bank/views/widgets/transaction_list_card.dart';

class ApprovedTransactions extends StatefulWidget {
  @override
  _ApprovedTransactionsState createState() => _ApprovedTransactionsState();
}

class _ApprovedTransactionsState extends State<ApprovedTransactions> {
// Properteis

  List<String> desc = [
    'Electronic Hyper Market',
    'Monthly Salary',
    'Mckdonalds'
  ];
  List<String> amounts = ['-4000', '+50000', '-2000'];
  List<IconData> icons = [
    Ionicons.laptop_outline,
    Ionicons.wallet_outline,
    Ionicons.fast_food_outline,
  ];

// Build

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
        child: Container(
      child: Column(
        children: [
          CustomHeading(
            title: 'Feb 25, 2018',
            trailing: IconButton(
              icon: Icon(
                Ionicons.calendar_outline,
                color: AppColors.primary,
              ),
              onPressed: () {},
            ),
          ),
          ListView.builder(
            itemCount: 3,
            shrinkWrap: true,
            physics: ClampingScrollPhysics(),
            itemBuilder: (BuildContext context, int index) {
              return TransactionListCard(
                description: desc[index],
                icon: icons[index],
                amount: amounts[index],
                isCredit: index == 1 ? true : false,
                status: 'Approved',
                transaction: '#43244$index',
              );
            },
          ),
        ],
      ),
    ));
  }
}
