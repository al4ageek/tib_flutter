import 'package:flutter/material.dart';
import 'package:tib_bank/config/constants/strings.dart';
import 'package:tib_bank/views/widgets/empty.dart';

class PendingTransactions extends StatefulWidget {
  @override
  _PendingTransactionsState createState() => _PendingTransactionsState();
}

class _PendingTransactionsState extends State<PendingTransactions> {
  @override
  Widget build(BuildContext context) {
    return EmptyArea(
      title: 'No Transacrtions Found',
      message: AppStrings.lorem,
    );
  }
}
