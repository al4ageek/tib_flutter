import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:tib_bank/controller/dashboard_ctrl.dart';

// ignore: must_be_immutable
class DashSlides extends StatelessWidget {
  DashboardController dashCtrl = Get.find();
  @override
  Widget build(BuildContext context) {
    return Container(
      child: CarouselSlider(
        options: CarouselOptions(
          height: 250.0,
          autoPlay: true,
          autoPlayAnimationDuration: Duration(milliseconds: 300),
        ),
        items: dashCtrl.slides.map((i) {
          return Builder(
            builder: (BuildContext context) {
              return Container(
                width: MediaQuery.of(context).size.width,
                margin: EdgeInsets.symmetric(horizontal: 5),
                child: i,
              );
            },
          );
        }).toList(),
      ),
    );
  }
}
