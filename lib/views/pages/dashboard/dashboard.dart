import 'package:flutter/material.dart';
import 'package:get/instance_manager.dart';
import 'package:ionicons/ionicons.dart';
import 'package:scroll_to_id/scroll_to_id.dart';
import 'package:tib_bank/config/constants/colors.dart';
import 'package:tib_bank/config/helper/helper.dart';
import 'package:tib_bank/config/helper/text_styles.dart';
import 'package:tib_bank/config/lang/app_locale.dart';
import 'package:tib_bank/controller/dashboard_ctrl.dart';
import 'package:tib_bank/views/pages/add_account/add_account.dart';
import 'package:tib_bank/views/pages/compare_rates/compare_rates.dart';
import 'package:tib_bank/views/pages/dashboard/slides.dart';
import 'package:tib_bank/views/pages/western_union/western_union.dart';
import 'package:tib_bank/views/widgets/account_list_card.dart';
import 'package:tib_bank/views/widgets/custom_heading.dart';
import 'package:tib_bank/views/widgets/operation_card.dart';
import 'package:tib_bank/views/widgets/payee_card.dart';
import 'package:tib_bank/views/widgets/transaction_list_card.dart';

class Dashboard extends StatefulWidget {
  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard>
    with SingleTickerProviderStateMixin {
// Properties

  DashboardController dashCtrl = Get.put(DashboardController());
  final scrollController = ScrollController();
  ScrollToId scrollToId;
  TabController tabCtrl;
  List<String> desc = [
    'Electronic Hyper Market',
    'Monthly Salary',
    'Mckdonalds'
  ];
  List<String> amounts = ['-4000', '+50000', '-2000'];
  List<IconData> icons = [
    Ionicons.laptop_outline,
    Ionicons.wallet_outline,
    Ionicons.fast_food_outline,
  ];
  var accCtrl = ScrollController();

// initstate

  @override
  void initState() {
    super.initState();
    scrollToId = ScrollToId(scrollController: scrollController);
    tabCtrl = TabController(length: 5, vsync: this);
    accCtrl.addListener(() {
      if (accCtrl.position.pixels == 100) {
        // You're at the top.
        print('now at 100');
        dashCtrl.tabIndex.value = 1;
      } else {
        // You're at the bottom.
      }
    });
  }

// Build
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Container(
            height: 60,
            decoration: BoxDecoration(
              border: Border(
                bottom: BorderSide(
                  color: Colors.grey.withAlpha(40),
                  width: 1,
                ),
              ),
            ),
            child: ClipRect(
              child: TabBar(
                indicatorColor: AppColors.primary,
                unselectedLabelColor: AppColors.text,
                labelColor: AppColors.primary,
                isScrollable: true,
                labelPadding: EdgeInsets.symmetric(horizontal: 20),
                labelStyle: AppText.labelText(),
                controller: tabCtrl,
                onTap: (e) {
                  scrollToId.animateTo(
                    e == 0
                        ? 'deals'
                        : e == 1
                            ? 'accounts'
                            : e == 2
                                ? 'operations'
                                : e == 3
                                    ? 'transactions'
                                    : 'quickpay',
                    duration: Duration(milliseconds: 500),
                    curve: Curves.ease,
                  );
                },
                tabs: [
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 0, vertical: 15),
                    child: Text(
                      AppLocalizations.of(context).translate('deals'),
                      style: AppText.tabsText(),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 0, vertical: 15),
                    child: Text(
                      AppLocalizations.of(context).translate('accounts'),
                      style: AppText.tabsText(),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 0, vertical: 15),
                    child: Text(
                      AppLocalizations.of(context).translate('operations'),
                      style: AppText.tabsText(),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 0, vertical: 15),
                    child: Text(
                      AppLocalizations.of(context).translate('transactions'),
                      style: AppText.tabsText(),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 0, vertical: 15),
                    child: Text(
                      AppLocalizations.of(context).translate('beneficieries'),
                      style: AppText.tabsText(),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Expanded(
            child: InteractiveScrollViewer(
              scrollToId: scrollToId,
              children: <ScrollContent>[
                ScrollContent(
                  id: 'deals',
                  child: Padding(
                    padding: EdgeInsets.only(top: 15),
                    child: DashSlides(),
                  ),
                ),
                ScrollContent(
                  id: 'accounts',
                  child: Container(
                    child: Column(
                      children: [
                        CustomHeading(
                          title: AppLocalizations.of(context)
                              .translate('accounts'),
                          trailing: MaterialButton(
                            onPressed: () {
                              Helper.to(AddAccount(), isModal: true);
                            },
                            child: Text(
                              AppLocalizations.of(context)
                                  .translate('add_new')
                                  .toUpperCase(),
                              style: AppText.buttonText(
                                color: AppColors.primary,
                              ),
                            ),
                          ),
                        ),
                        Container(
                          height: 300,
                          child: PageView(
                            pageSnapping: false,
                            scrollDirection: Axis.horizontal,
                            onPageChanged: (e) {
                              dashCtrl.tabIndex.value = 2;
                            },
                            physics: ClampingScrollPhysics(),
                            children: [
                              Column(
                                children: [
                                  AccountListCard(
                                    title: 'Saving Account',
                                    accountNo: '1233 **** **** 3442',
                                    amount: '130,000 IQD',
                                    icon: Ionicons.server,
                                    image: 'assets/img/account_bg.png',
                                    trailing: MaterialButton(
                                      onPressed: () {},
                                      child: Text(
                                        AppLocalizations.of(context)
                                            .translate('view_details')
                                            .toUpperCase(),
                                        style: AppText.buttonText(
                                          color: AppColors.primary,
                                        ),
                                      ),
                                    ),
                                  ),
                                  AccountListCard(
                                    title: 'Master Card',
                                    accountNo: '1233 **** **** 3442',
                                    amount: '300,000 IQD',
                                    isCard: true,
                                    cardProgress: 0.5,
                                    image: 'assets/img/account_bg.png',
                                    trailing: MaterialButton(
                                      onPressed: () {},
                                      child: Text(
                                        AppLocalizations.of(context)
                                            .translate('view_details')
                                            .toUpperCase(),
                                        style: AppText.buttonText(
                                          color: AppColors.primary,
                                        ),
                                      ),
                                    ),
                                  ),
                                  AccountListCard(
                                    title: 'eSaver Lite',
                                    accountNo: 'Free Activation',
                                    amount: 'Multiple Benefits',
                                    icon: Ionicons.archive_outline,
                                    image: 'assets/img/account_bg_two.png',
                                    trailing: MaterialButton(
                                      onPressed: () {},
                                      child: Text(
                                        AppLocalizations.of(context)
                                            .translate('activate')
                                            .toUpperCase(),
                                        style: AppText.buttonText(
                                          color: AppColors.secondary,
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              Column(
                                children: [
                                  AccountListCard(
                                    title: 'Saving Account',
                                    accountNo: '1233 **** **** 3442',
                                    amount: '220,000 IQD',
                                    icon: Ionicons.server,
                                    image: 'assets/img/account_bg.png',
                                    trailing: MaterialButton(
                                      onPressed: () {},
                                      child: Text(
                                        AppLocalizations.of(context)
                                            .translate('view_details')
                                            .toUpperCase(),
                                        style: AppText.buttonText(
                                          color: AppColors.primary,
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ),
                ScrollContent(
                  id: 'operations',
                  child: Container(
                    padding: EdgeInsets.fromLTRB(10, 0, 10, 15),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        CustomHeading(
                          title: AppLocalizations.of(context)
                              .translate('operations'),
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 15),
                        ),
                        Row(
                          children: [
                            Expanded(
                              child: OperationCard(
                                color: AppColors.primary,
                                icon: Ionicons.wallet_outline,
                                title: AppLocalizations.of(context)
                                    .translate('send_money'),
                              ),
                            ),
                            Expanded(
                              child: OperationCard(
                                color: AppColors.secondary,
                                icon: Ionicons.earth_outline,
                                title: AppLocalizations.of(context)
                                    .translate('western_union'),
                                onTap: () {
                                  Helper.to(WesternUnion());
                                },
                              ),
                            ),
                          ],
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 10),
                        ),
                        Row(
                          children: [
                            Expanded(
                              child: OperationCard(
                                color: AppColors.secondary,
                                icon: Ionicons.diamond_outline,
                                title: AppLocalizations.of(context)
                                    .translate('exchange_rates'),
                                onTap: () {
                                  Helper.to(CompareRates());
                                },
                              ),
                            ),
                            Expanded(
                              child: OperationCard(
                                color: AppColors.secondary,
                                icon: Ionicons.list_outline,
                                title: AppLocalizations.of(context)
                                    .translate('transactions'),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
                ScrollContent(
                  id: 'transactions',
                  child: Container(
                    child: Column(
                      children: [
                        CustomHeading(
                          title: AppLocalizations.of(context)
                              .translate('transactions'),
                          trailing: MaterialButton(
                            onPressed: () {},
                            child: Text(
                              AppLocalizations.of(context)
                                  .translate('view_all')
                                  .toUpperCase(),
                              style: AppText.buttonText(
                                color: AppColors.primary,
                              ),
                            ),
                          ),
                        ),
                        ListView.builder(
                          itemCount: 3,
                          shrinkWrap: true,
                          physics: ClampingScrollPhysics(),
                          itemBuilder: (BuildContext context, int index) {
                            return TransactionListCard(
                              description: desc[index],
                              icon: icons[index],
                              amount: amounts[index],
                              isCredit: index == 1 ? true : false,
                              status: 'Approved',
                              transaction: '#43244$index',
                            );
                          },
                        ),
                      ],
                    ),
                  ),
                ),
                ScrollContent(
                  id: 'quickpay',
                  child: Container(
                    padding: EdgeInsets.only(bottom: 400),
                    child: Column(
                      children: [
                        CustomHeading(
                          title: AppLocalizations.of(context)
                              .translate('beneficieries'),
                          trailing: MaterialButton(
                            onPressed: () {},
                            child: Text(
                              AppLocalizations.of(context)
                                  .translate('add_new')
                                  .toUpperCase(),
                              style: AppText.buttonText(
                                color: AppColors.primary,
                              ),
                            ),
                          ),
                        ),
                        Container(
                          height: 200,
                          margin: EdgeInsets.only(bottom: 50),
                          child: ListView.builder(
                            itemCount: 4,
                            shrinkWrap: true,
                            physics: ClampingScrollPhysics(),
                            scrollDirection: Axis.horizontal,
                            padding: EdgeInsets.symmetric(horizontal: 15),
                            itemBuilder: (BuildContext context, int index) {
                              return Container(
                                margin: EdgeInsets.only(right: 10),
                                child: PayeeCard(
                                  username: 'John Doe',
                                  bank: 'Emirates NBD',
                                  image: 'https://i.pravatar.cc/10$index',
                                ),
                              );
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
