import 'package:flutter/material.dart';
import 'package:ionicons/ionicons.dart';
import 'package:tib_bank/config/constants/colors.dart';
import 'package:tib_bank/config/helper/text_styles.dart';
import 'package:tib_bank/config/lang/app_locale.dart';
import 'package:tib_bank/views/widgets/app_bar.dart';
import 'package:tib_bank/views/widgets/transaction_list_card.dart';

class Statement extends StatefulWidget {
  @override
  _StatementState createState() => _StatementState();
}

class _StatementState extends State<Statement> {
// Properteis

  List<String> desc = [
    'Electronic Hyper Market',
    'Monthly Salary',
    'Mckdonalds'
  ];
  List<String> amounts = ['-4000', '+50000', '-2000'];
  List<IconData> icons = [
    Ionicons.laptop_outline,
    Ionicons.wallet_outline,
    Ionicons.fast_food_outline,
  ];

// Build

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppbar(
        title: AppLocalizations.of(context).translate('statement'),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.symmetric(horizontal: 15, vertical: 15),
              child: Row(
                children: [
                  Expanded(
                    child: Container(
                      decoration: BoxDecoration(
                        border: Border(
                          bottom: BorderSide(
                            color: AppColors.primary,
                            width: 2,
                          ),
                        ),
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            AppLocalizations.of(context)
                                .translate('from')
                                .toUpperCase(),
                            style: AppText.content(color: AppColors.primary),
                          ),
                          Text(
                            '30 Feb 2018',
                            style: AppText.contentBold(),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Container(
                    width: 40,
                    height: 40,
                    margin: EdgeInsets.symmetric(horizontal: 25),
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: AppColors.primary,
                    ),
                    child: IconButton(
                      onPressed: () {},
                      icon: Icon(
                        Ionicons.arrow_forward,
                      ),
                      color: AppColors.white,
                    ),
                  ),
                  Expanded(
                    child: Container(
                      decoration: BoxDecoration(
                        border: Border(
                          bottom: BorderSide(
                            color: AppColors.primary,
                            width: 2,
                          ),
                        ),
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            AppLocalizations.of(context)
                                .translate('to')
                                .toUpperCase(),
                            style: AppText.content(color: AppColors.primary),
                          ),
                          Text(
                            '30 Feb 2018',
                            style: AppText.contentBold(),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            ListView.builder(
              itemCount: 3,
              shrinkWrap: true,
              physics: ClampingScrollPhysics(),
              itemBuilder: (BuildContext context, int index) {
                return TransactionListCard(
                  description: desc[index],
                  icon: icons[index],
                  amount: amounts[index],
                  isCredit: index == 1 ? true : false,
                  status: 'Approved',
                  transaction: '#43244$index',
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
