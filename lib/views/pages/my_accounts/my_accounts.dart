import 'package:flutter/material.dart';
import 'package:tib_bank/config/constants/colors.dart';
import 'package:tib_bank/config/helper/text_styles.dart';
import 'package:tib_bank/config/lang/app_locale.dart';
import 'package:tib_bank/views/pages/my_accounts/accounts.dart';
import 'package:tib_bank/views/pages/my_accounts/cards.dart';

class MyAccounts extends StatefulWidget {
  @override
  _MyAccountsState createState() => _MyAccountsState();
}

class _MyAccountsState extends State<MyAccounts> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      initialIndex: 0,
      child: Container(
        color: AppColors.white,
        child: Column(
          children: [
            Container(
              height: 60,
              decoration: BoxDecoration(
                border: Border(
                  bottom: BorderSide(
                    color: Colors.grey.withAlpha(40),
                    width: 1,
                  ),
                ),
              ),
              child: TabBar(
                indicatorColor: AppColors.primary,
                unselectedLabelColor: AppColors.text,
                labelColor: AppColors.text,
                labelStyle: AppText.labelText(),
                isScrollable: true,
                tabs: [
                  Container(
                    width: 120,
                    padding: EdgeInsets.symmetric(horizontal: 0, vertical: 15),
                    child: Center(
                      child: Text(
                        AppLocalizations.of(context).translate('accounts'),
                        style: AppText.tabsText(),
                      ),
                    ),
                  ),
                  Container(
                    width: 120,
                    padding: EdgeInsets.symmetric(horizontal: 0, vertical: 15),
                    child: Center(
                      child: Text(
                        AppLocalizations.of(context).translate('cards'),
                        style: AppText.tabsText(),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Expanded(
              child: Container(
                color: AppColors.white,
                child: TabBarView(
                  children: [
                    Accounts(),
                    Cards(),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
