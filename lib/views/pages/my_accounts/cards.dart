import 'package:flutter/material.dart';
import 'package:tib_bank/config/constants/colors.dart';
import 'package:tib_bank/views/widgets/credit_card.dart';

class Cards extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      shrinkWrap: true,
      physics: ClampingScrollPhysics(),
      itemCount: 2,
      padding: EdgeInsets.symmetric(vertical: 15),
      itemBuilder: (BuildContext context, int index) {
        return CreditCard(
          color: index == 0 ? AppColors.primary : AppColors.secondary,
        );
      },
    );
  }
}
