import 'package:flutter/material.dart';
import 'package:ionicons/ionicons.dart';
import 'package:tib_bank/config/constants/colors.dart';
import 'package:tib_bank/config/helper/helper.dart';
import 'package:tib_bank/config/helper/text_styles.dart';
import 'package:tib_bank/config/lang/app_locale.dart';
import 'package:tib_bank/views/pages/beneficieries/beneficieries.dart';
import 'package:tib_bank/views/pages/rate_calculatro/rate_calculator.dart';
import 'package:tib_bank/views/pages/send_money/send_money.dart';
import 'package:tib_bank/views/pages/statement/statment.dart';
import 'package:tib_bank/views/pages/transactions/transactions.dart';
import 'package:tib_bank/views/widgets/app_bar.dart';
import 'package:tib_bank/views/widgets/credit_card.dart';
import 'package:tib_bank/views/widgets/custom_heading.dart';
import 'package:tib_bank/views/widgets/list_item.dart';
import 'package:tib_bank/views/widgets/transaction_list_card.dart';

class AccountDetails extends StatefulWidget {
  @override
  _AccountDetailsState createState() => _AccountDetailsState();
}

class _AccountDetailsState extends State<AccountDetails> {
// Properteis

  List<String> desc = [
    'Electronic Hyper Market',
    'Monthly Salary',
    'Mckdonalds'
  ];
  List<String> amounts = ['-4000', '+50000', '-2000'];
  List<IconData> icons = [
    Ionicons.laptop_outline,
    Ionicons.wallet_outline,
    Ionicons.fast_food_outline,
  ];

// Build

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppbar(
        title: 'Account Details',
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.symmetric(vertical: 15),
          child: Column(
            children: [
              CreditCard(
                color: AppColors.primary,
              ),
              CustomHeading(
                title: AppLocalizations.of(context).translate('operations'),
              ),
              MenuListItem(
                title: AppLocalizations.of(context).translate('send_money'),
                icon: Ionicons.wallet_outline,
                onTap: () {
                  Helper.to(SendMoney());
                },
              ),
              MenuListItem(
                title: AppLocalizations.of(context).translate('exchange_rates'),
                icon: Ionicons.diamond_outline,
                onTap: () {
                  Helper.to(RateCalculator());
                },
              ),
              MenuListItem(
                title: AppLocalizations.of(context).translate('transactions'),
                icon: Ionicons.list_outline,
                onTap: () {
                  Helper.to(Transactions());
                },
              ),
              MenuListItem(
                title: AppLocalizations.of(context).translate('statement'),
                icon: Ionicons.document_attach_outline,
                onTap: () {
                  Helper.to(Statement());
                },
              ),
              MenuListItem(
                title: AppLocalizations.of(context).translate('beneficieries'),
                icon: Ionicons.umbrella_outline,
                onTap: () {
                  Helper.to(Beneficieries());
                },
              ),
              CustomHeading(
                title: AppLocalizations.of(context).translate('transactions'),
                trailing: MaterialButton(
                  onPressed: () {},
                  child: Text(
                    AppLocalizations.of(context)
                        .translate('view_all')
                        .toUpperCase(),
                    style: AppText.buttonText(
                      color: AppColors.primary,
                    ),
                  ),
                ),
              ),
              ListView.builder(
                itemCount: 3,
                shrinkWrap: true,
                physics: ClampingScrollPhysics(),
                itemBuilder: (BuildContext context, int index) {
                  return TransactionListCard(
                    description: desc[index],
                    icon: icons[index],
                    amount: amounts[index],
                    isCredit: index == 1 ? true : false,
                    status: 'Approved',
                    transaction: '#43244$index',
                  );
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
