import 'package:flutter/material.dart';
import 'package:ionicons/ionicons.dart';
import 'package:tib_bank/config/constants/colors.dart';
import 'package:tib_bank/config/constants/strings.dart';
import 'package:tib_bank/config/helper/helper.dart';
import 'package:tib_bank/views/pages/beneficieries/beneficieries.dart';
import 'package:tib_bank/views/pages/rate_calculatro/rate_calculator.dart';
import 'package:tib_bank/views/pages/send_money/send_money.dart';
import 'package:tib_bank/views/pages/statement/statment.dart';
import 'package:tib_bank/views/widgets/service_card.dart';

class Services extends StatefulWidget {
  @override
  _ServicesState createState() => _ServicesState();
}

class _ServicesState extends State<Services> {
// Props

  List<String> services = [
    'Request Checkbook',
    'Rate Calculator',
    'Send Money',
    'Compare Rates',
    'Beneficiries',
    'Statment',
  ];

  List<IconData> icons = [
    Ionicons.card_outline,
    Ionicons.reload_outline,
    Ionicons.wallet_outline,
    Ionicons.rocket_outline,
    Ionicons.umbrella_outline,
    Ionicons.document_attach_outline,
  ];

// Redirect

  redirect(int index) {
    if (index == 0) {
    } else if (index == 1) {
      Helper.to(RateCalculator());
    } else if (index == 2) {
      Helper.to(SendMoney());
    } else if (index == 3) {
      Helper.to(RateCalculator());
    } else if (index == 4) {
      Helper.to(Beneficieries());
    } else if (index == 5) {
      Helper.to(Statement());
    } else {}
  }

// Build

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: services.length,
      itemBuilder: (BuildContext context, int index) {
        return ServiceCard(
          color: index.isEven ? AppColors.secondary : AppColors.primary,
          title: services[index],
          icon: icons[index],
          description: AppStrings.lorem,
          reverseCircle: index.isEven ? true : false,
          onTap: () {
            redirect(index);
          },
        );
      },
    );
  }
}
