import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ionicons/ionicons.dart';
import 'package:tib_bank/config/constants/colors.dart';
import 'package:tib_bank/config/constants/images.dart';
import 'package:tib_bank/config/helper/helper.dart';
import 'package:tib_bank/config/helper/text_styles.dart';
import 'package:tib_bank/config/lang/app_locale.dart';
import 'package:tib_bank/controller/home_ctrl.dart';
import 'package:tib_bank/controller/login_ctrl.dart';
import 'package:tib_bank/views/pages/home/home.dart';
import 'package:tib_bank/views/pages/schedule_call/schedule_call.dart';
import 'package:tib_bank/views/pages/video_banking/video_banking.dart';
import 'package:tib_bank/views/widgets/app_bar.dart';
import 'package:tib_bank/views/widgets/app_button.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
// Properties

  LoginController loginCtrl = Get.put(LoginController());
  HomeController homeController = Get.put(HomeController(), permanent: true);

// Build
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.primary,
      appBar: CustomAppbar(
        title: AppLocalizations.of(context).translate('login'),
        bgColor: Colors.transparent,
        elevation: 0,
        textColor: Colors.white,
        centerTitle: true,
        brightness: Brightness.dark,
      ),
      body: Stack(
        children: [
          Positioned(
            left: 15,
            top: 12,
            child: Container(
              width: MediaQuery.of(context).size.width - 30,
              height: MediaQuery.of(context).size.height,
              decoration: BoxDecoration(
                color: Colors.white.withAlpha(80),
                borderRadius: BorderRadius.circular(10),
              ),
            ),
          ),
          Positioned(
            left: 30,
            top: 0,
            child: Container(
              width: MediaQuery.of(context).size.width - 60,
              height: MediaQuery.of(context).size.height - 140,
              decoration: BoxDecoration(
                color: Colors.white.withAlpha(80),
                borderRadius: BorderRadius.circular(10),
              ),
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(10),
            ),
            child: SingleChildScrollView(
              child: Container(
                padding: EdgeInsets.all(25),
                child: Column(
                  children: [
                    Container(
                      margin: EdgeInsets.fromLTRB(15, 40, 15, 30),
                      width: 250,
                      child: Image.asset(
                        AppImages.logo,
                        fit: BoxFit.fitWidth,
                      ),
                    ),
                    Form(
                      key: loginCtrl.key,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Center(
                            child: Text(
                              AppLocalizations.of(context)
                                  .translate('enter_info')
                                  .toUpperCase(),
                              style: AppText.content(),
                              textAlign: TextAlign.center,
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(bottom: 30),
                          ),
                          Text(
                            AppLocalizations.of(context)
                                .translate('username_email')
                                .toUpperCase(),
                            style: AppText.labelText(),
                          ),
                          TextFormField(
                            decoration: InputDecoration(
                              hintText: AppLocalizations.of(context)
                                  .translate('enter_email'),
                              hintStyle: AppText.content(),
                              icon: Icon(
                                Ionicons.mail_outline,
                              ),
                            ),
                            keyboardType: TextInputType.emailAddress,
                            focusNode: loginCtrl.nameFocus,
                            textInputAction: TextInputAction.next,
                            validator: Helper.validateEmpty,
                            controller: loginCtrl.nameCtrl,
                            onFieldSubmitted: (term) {
                              Helper.fieldFocusChange(
                                context,
                                loginCtrl.nameFocus,
                                loginCtrl.passFocus,
                              );
                            },
                          ),
                          Padding(
                            padding: EdgeInsets.only(top: 30),
                          ),
                          Text(
                            AppLocalizations.of(context)
                                .translate('password')
                                .toUpperCase(),
                            style: AppText.labelText(),
                          ),
                          TextFormField(
                            decoration: InputDecoration(
                              hintText: AppLocalizations.of(context)
                                  .translate('enter_password'),
                              hintStyle: AppText.content(),
                              icon: Icon(
                                Ionicons.lock_closed_outline,
                              ),
                              suffix: IconButton(
                                icon: Icon(
                                  loginCtrl.pwdVisible.value
                                      ? Ionicons.eye_off
                                      : Ionicons.eye,
                                  color: AppColors.primary,
                                ),
                                onPressed: () {
                                  setState(() {
                                    loginCtrl.pwdVisible.value =
                                        !loginCtrl.pwdVisible.value;
                                  });
                                },
                              ),
                            ),
                            obscureText: !loginCtrl.pwdVisible.value,
                            focusNode: loginCtrl.passFocus,
                            validator: Helper.validateEmpty,
                            controller: loginCtrl.passCtrl,
                            onFieldSubmitted: (val) {},
                          ),
                          Padding(
                            padding: EdgeInsets.only(top: 30),
                          ),
                          Row(
                            children: [
                              Expanded(
                                child: CustomButton(
                                  title: AppLocalizations.of(context)
                                      .translate('login'),
                                  icon: Ionicons.arrow_forward,
                                  backgroundColor: AppColors.secondary,
                                  textColor: Colors.white,
                                  radius: 5,
                                  onTap: () {
                                    //  loginCtrl.doLogin();
                                    Helper.showModal(
                                      context: context,
                                      icon: Ionicons.server_outline,
                                      color: AppColors.danger,
                                      message:
                                          'Some Error Occured from the server side please try again!',
                                      title: 'Something went wrong',
                                    );
                                  },
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.symmetric(horizontal: 5),
                              ),
                              CustomButton(
                                icon: Ionicons.happy_outline,
                                onTap: () {},
                                backgroundColor: Colors.white,
                                borderColor: AppColors.secondary,
                                hasBorder: true,
                                textColor: AppColors.secondary,
                                width: 50,
                                radius: 5,
                              ),
                            ],
                          ),
                          Padding(
                            padding: EdgeInsets.only(top: 20),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              InkWell(
                                child: Text(
                                  AppLocalizations.of(context)
                                      .translate('forgot_password'),
                                  style: AppText.content(),
                                ),
                              ),
                              RichText(
                                text: TextSpan(
                                  children: [
                                    TextSpan(
                                        text: AppLocalizations.of(context)
                                            .translate('need_to'),
                                        style: AppText.content()),
                                    WidgetSpan(
                                      child: Padding(
                                        padding:
                                            EdgeInsets.symmetric(horizontal: 3),
                                      ),
                                    ),
                                    TextSpan(
                                      text: AppLocalizations.of(context)
                                          .translate('signup')
                                          .toUpperCase(),
                                      style: AppText.contentUnderline(),
                                      recognizer: TapGestureRecognizer()
                                        ..onTap = () {},
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 40),
                      width: MediaQuery.of(context).size.width,
                      child: Row(
                        children: [
                          Expanded(
                            child: CustomButton(
                              icon: Ionicons.videocam,
                              onTap: () {
                                Helper.to(VideoBanking());
                              },
                              backgroundColor: AppColors.secondary,
                              hasBorder: false,
                              textColor: AppColors.white,
                              radius: 5,
                              title: AppLocalizations.of(context)
                                  .translate('video_banking'),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.symmetric(horizontal: 5),
                          ),
                          Expanded(
                            child: CustomButton(
                              icon: Ionicons.calendar_outline,
                              onTap: () {
                                Helper.to(ScheduleCall());
                              },
                              backgroundColor: AppColors.white,
                              hasBorder: true,
                              borderColor: AppColors.secondary,
                              textColor: AppColors.secondary,
                              radius: 5,
                              title: AppLocalizations.of(context)
                                  .translate('book_appointment'),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
            margin: EdgeInsets.only(top: 25),
          ),
        ],
      ),
    );
  }
}
