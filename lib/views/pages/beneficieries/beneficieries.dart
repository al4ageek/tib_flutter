import 'package:flutter/material.dart';
import 'package:tib_bank/config/lang/app_locale.dart';
import 'package:tib_bank/views/widgets/app_bar.dart';
import 'package:tib_bank/views/widgets/payee_card.dart';

class Beneficieries extends StatefulWidget {
  @override
  _BeneficieriesState createState() => _BeneficieriesState();
}

class _BeneficieriesState extends State<Beneficieries> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppbar(
        title: AppLocalizations.of(context).translate('beneficieries'),
      ),
      body: GridView.builder(
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
          crossAxisSpacing: 10,
        ),
        itemCount: 8,
        padding: EdgeInsets.symmetric(horizontal: 10),
        itemBuilder: (BuildContext context, int index) {
          return Container(
            child: PayeeCard(
              username: 'John Doe',
              bank: 'Emirates NBD',
              image: 'https://i.pravatar.cc/10$index',
            ),
          );
        },
      ),
    );
  }
}
