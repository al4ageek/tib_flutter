import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ionicons/ionicons.dart';
import 'package:tib_bank/config/constants/colors.dart';
import 'package:tib_bank/config/helper/helper.dart';
import 'package:tib_bank/config/helper/text_styles.dart';
import 'package:tib_bank/config/lang/app_locale.dart';
import 'package:tib_bank/controller/home_ctrl.dart';
import 'package:tib_bank/views/pages/dashboard/dashboard.dart';
import 'package:tib_bank/views/pages/more/more.dart';
import 'package:tib_bank/views/pages/my_accounts/my_accounts.dart';
import 'package:tib_bank/views/pages/services/services.dart';
import 'package:tib_bank/views/widgets/app_bar.dart';
import 'package:tib_bank/views/widgets/menu_icon.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {// Properteis

  HomeController homeCtrl = Get.find();
 
// Build

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => Scaffold(
        appBar: homeCtrl.currentPage.value == 0
            ? AppBar(
                elevation: 0,
                backgroundColor: Colors.transparent,
                flexibleSpace: ClipRect(
                  child: BackdropFilter(
                    filter: ImageFilter.blur(sigmaX: 5, sigmaY: 5),
                    child: Container(
                      color: Colors.transparent,
                    ),
                  ),
                ),
                title: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Container(
                      width: 50,
                      height: 50,
                      margin: Helper.isRtl(context)
                          ? EdgeInsets.only(left: 15)
                          : EdgeInsets.only(right: 15),
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                      ),
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(10),
                        child: Image.asset(
                          'assets/img/user.png',
                          fit: BoxFit.contain,
                        ),
                      ),
                    ),
                    Expanded(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            AppLocalizations.of(context).translate(
                              'welcome',
                            ),
                            style: AppText.content(),
                          ),
                          Text(
                            homeCtrl.user.firstName + " " + homeCtrl.user.middleName + " " + homeCtrl.user.lastName ,
                            style: AppText.secondaryHeading(
                                color: AppColors.primary),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
                actions: [
                  IconButton(
                    icon: Icon(
                      Ionicons.chatbubble_outline,
                    ),
                    onPressed: () {},
                  ),
                  IconButton(
                    icon: Icon(
                      Ionicons.notifications_outline,
                    ),
                    onPressed: () {},
                  ),
                ],
              )
            : CustomAppbar(
                title: homeCtrl.currentPage.value == 0
                    ? AppLocalizations.of(context).translate('dashboard')
                    : homeCtrl.currentPage.value == 1
                        ? AppLocalizations.of(context).translate('my_accounts')
                        : homeCtrl.currentPage.value == 2
                            ? AppLocalizations.of(context).translate('services')
                            : homeCtrl.currentPage.value == 3
                                ? AppLocalizations.of(context).translate('more')
                                : "",
                actions: [
                  IconButton(
                    icon: Icon(
                      Ionicons.chatbubble_outline,
                    ),
                    onPressed: () {},
                  ),
                  IconButton(
                    icon: Icon(
                      Ionicons.notifications_outline,
                    ),
                    onPressed: () {},
                  ),
                ],
              ),
        body:
            PageStorage(child: homeCtrl.currentScreen, bucket: homeCtrl.bucket),
        bottomNavigationBar: Directionality(
          textDirection: TextDirection.ltr,
          child: Container(
            height: 100,
            child: BottomAppBar(
              shape: CircularNotchedRectangle(),
              notchMargin: 10,
              color: AppColors.white,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    padding: EdgeInsets.symmetric(vertical: 15, horizontal: 10),
                    margin: EdgeInsets.only(right: 25),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        MenuIcon(
                          icon: homeCtrl.currentPage.value == 0
                              ? Ionicons.pie_chart
                              : Ionicons.pie_chart_outline,
                          color: homeCtrl.currentPage.value == 0
                              ? AppColors.primary
                              : AppColors.text,
                          onTap: () {
                            homeCtrl.setActivePage(Dashboard(), 0);
                          },
                        ),
                        MenuIcon(
                          icon: homeCtrl.currentPage.value == 1
                              ? Ionicons.wallet
                              : Ionicons.wallet_outline,
                          color: homeCtrl.currentPage.value == 1
                              ? AppColors.primary
                              : AppColors.text,
                          onTap: () {
                            homeCtrl.setActivePage(MyAccounts(), 1);
                          },
                        ),
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(vertical: 15, horizontal: 10),
                    margin: EdgeInsets.only(left: 25),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        MenuIcon(
                          icon: homeCtrl.currentPage.value == 2
                              ? Ionicons.briefcase
                              : Ionicons.briefcase_outline,
                          color: homeCtrl.currentPage.value == 2
                              ? AppColors.primary
                              : AppColors.text,
                          onTap: () {
                            homeCtrl.setActivePage(Services(), 2);
                          },
                        ),
                        MenuIcon(
                          icon: homeCtrl.currentPage.value == 3
                              ? Ionicons.ellipsis_vertical
                              : Ionicons.ellipsis_vertical_outline,
                          color: homeCtrl.currentPage.value == 3
                              ? AppColors.primary
                              : AppColors.text,
                          onTap: () {
                            homeCtrl.setActivePage(More(), 3);
                          },
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
        floatingActionButton: FloatingActionButton(
          child: Icon(
            Ionicons.add,
            color: Colors.white,
          ),
          onPressed: () {},
        ),
        floatingActionButtonLocation:
            FloatingActionButtonLocation.miniCenterDocked,
      ),
    );
  }
}
