import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ionicons/ionicons.dart';
import 'package:tib_bank/config/constants/colors.dart';
import 'package:tib_bank/config/helper/helper.dart';
import 'package:tib_bank/config/helper/text_styles.dart';
import 'package:tib_bank/config/lang/app_language.dart';
import 'package:tib_bank/config/lang/app_locale.dart';
import 'package:tib_bank/views/pages/dashboard/slides.dart';
import 'package:tib_bank/views/pages/profile/profile.dart';
import 'package:tib_bank/views/pages/settings/settings.dart';
import 'package:tib_bank/views/pages/transactions/transactions.dart';
import 'package:tib_bank/views/widgets/custom_heading.dart';
import 'package:tib_bank/views/widgets/list_item.dart';

class More extends StatefulWidget {
  @override
  _MoreState createState() => _MoreState();
}

class _MoreState extends State<More> {
// Propertes

  AppLanguage appLanguage = Get.find();

// Show Lnaguage Sheet

  changeLanguage() {
    Helper.showSheet(
        context,
        'Change Language',
        Container(
          padding: EdgeInsets.all(25),
          child: Column(
            children: [
              ListTile(
                leading: Icon(
                  Helper.isRtl(context)
                      ? Ionicons.radio_button_off
                      : Ionicons.radio_button_on,
                  color: AppColors.primary,
                  size: 20,
                ),
                onTap: () {
                  appLanguage.changeLanguage(Locale("en"));
                  Navigator.pop(context);
                },
                title: Text(
                  'English',
                  style: AppText.content(),
                ),
              ),
              ListTile(
                onTap: () {
                  appLanguage.changeLanguage(Locale("ar"));
                  Navigator.pop(context);
                },
                leading: Icon(
                  Helper.isRtl(context)
                      ? Ionicons.radio_button_on
                      : Ionicons.radio_button_off,
                  color: AppColors.primary,
                  size: 20,
                ),
                title: Text(
                  'العربي',
                  style: AppText.content(),
                ),
              ),
            ],
          ),
        ));
  }

// Build
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          MenuListItem(
            title: AppLocalizations.of(context).translate('settings'),
            icon: Ionicons.settings_outline,
            onTap: () {
              Helper.to(Settings());
            },
          ),
          MenuListItem(
            title: AppLocalizations.of(context).translate('transactions'),
            icon: Ionicons.list_outline,
            onTap: () {
              Helper.to(Transactions());
            },
          ),
          MenuListItem(
            title: AppLocalizations.of(context).translate('language'),
            icon: Ionicons.language_outline,
            onTap: changeLanguage,
          ),
          MenuListItem(
            title: AppLocalizations.of(context).translate('profile'),
            icon: Ionicons.person_outline,
            onTap: () {
              Helper.to(Profile());
            },
          ),
          MenuListItem(
            title: AppLocalizations.of(context).translate('logout'),
            icon: Ionicons.log_out_outline,
          ),
        ],
      ),
    );
  }
}
