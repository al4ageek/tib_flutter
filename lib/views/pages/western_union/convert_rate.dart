import 'package:country_code_picker/country_code_picker.dart';
import 'package:flutter/material.dart';
import 'package:ionicons/ionicons.dart';
import 'package:tib_bank/config/constants/colors.dart';
import 'package:tib_bank/config/helper/text_styles.dart';
import 'package:tib_bank/config/helper/ui_styles.dart';
import 'package:tib_bank/config/lang/app_locale.dart';

class ConvertRate extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(15),
      child: Column(
        children: [
          Row(
            children: [
              Expanded(
                child: Container(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        AppLocalizations.of(context)
                            .translate('you_have')
                            .toUpperCase(),
                        style: AppText.content(color: AppColors.primary),
                      ),
                      TextFormField(
                        decoration: InputDecoration(
                          hintText: AppLocalizations.of(context)
                              .translate('enter_amount'),
                          hintStyle: AppText.secondaryHeading(),
                        ),
                        keyboardType: TextInputType.number,
                        textInputAction: TextInputAction.next,
                        // validator: Helper.validateEmpty,
                        onFieldSubmitted: (term) {},
                      ),
                    ],
                  ),
                ),
              ),
              Container(
                width: 200,
                margin: EdgeInsets.only(top: 25),
                child: CountryCodePicker(
                  padding: EdgeInsets.all(15),
                  onChanged: print,
                  initialSelection: 'US',
                  boxDecoration: UIStyles.cardDecoration(
                    radius: 10,
                  ),
                  showFlagMain: true,
                  showFlag: true,
                  showCountryOnly: false,
                  showOnlyCountryWhenClosed: true,
                  alignLeft: true,
                  hideMainText: false,
                  dialogTextStyle: AppText.content(),
                  closeIcon: Icon(
                    Ionicons.close,
                    color: AppColors.danger,
                    size: 30,
                  ),
                  barrierColor: Colors.black.withAlpha(70),
                  dialogSize: Size.fromHeight(500),
                  searchDecoration: InputDecoration(
                    contentPadding: EdgeInsets.all(5),
                  ),
                ),
                decoration: BoxDecoration(
                  color: AppColors.background,
                  borderRadius: BorderRadius.circular(10),
                ),
              ),
            ],
          ),
          Container(
            margin: EdgeInsets.symmetric(vertical: 25),
            child: Container(
              width: 40,
              height: 40,
              margin: EdgeInsets.symmetric(horizontal: 25),
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: AppColors.primary,
              ),
              child: IconButton(
                onPressed: () {},
                icon: Icon(
                  Ionicons.swap_vertical,
                ),
                color: AppColors.white,
              ),
            ),
          ),
          Row(
            children: [
              Expanded(
                child: Container(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        AppLocalizations.of(context)
                            .translate('you_get')
                            .toUpperCase(),
                        style: AppText.content(color: AppColors.primary),
                      ),
                      TextFormField(
                        decoration: InputDecoration(
                          hintText: AppLocalizations.of(context)
                              .translate('enter_amount'),
                          hintStyle: AppText.secondaryHeading(),
                        ),
                        keyboardType: TextInputType.number,
                        textInputAction: TextInputAction.next,
                        // validator: Helper.validateEmpty,
                        onFieldSubmitted: (term) {},
                      ),
                    ],
                  ),
                ),
              ),
              Container(
                width: 200,
                margin: EdgeInsets.only(top: 25),
                child: CountryCodePicker(
                  onChanged: print,
                  initialSelection: 'AE',
                  favorite: [
                    '+971',
                    'AE',
                  ],
                  boxDecoration: UIStyles.cardDecoration(
                    radius: 10,
                  ),
                  showFlagMain: true,
                  showFlag: true,
                  showCountryOnly: false,
                  showOnlyCountryWhenClosed: true,
                  alignLeft: true,
                  hideMainText: false,
                  dialogTextStyle: AppText.content(),
                  closeIcon: Icon(
                    Ionicons.close,
                    color: AppColors.danger,
                    size: 30,
                  ),
                  barrierColor: Colors.black.withAlpha(70),
                  dialogSize: Size.fromHeight(500),
                  searchDecoration: InputDecoration(
                    contentPadding: EdgeInsets.all(5),
                  ),
                ),
                decoration: BoxDecoration(
                  color: AppColors.background,
                  borderRadius: BorderRadius.circular(10),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
