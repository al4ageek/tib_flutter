import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:im_stepper/stepper.dart';
import 'package:ionicons/ionicons.dart';
import 'package:tib_bank/config/constants/colors.dart';
import 'package:tib_bank/controller/stepper_ctrl.dart';
import 'package:tib_bank/views/pages/western_union/review_amount.dart';
import 'package:tib_bank/views/widgets/app_bar.dart';

import 'choose_account.dart';
import 'convert_rate.dart';

class WesternUnion extends StatefulWidget {
  @override
  _WesternUnionState createState() => _WesternUnionState();
}

class _WesternUnionState extends State<WesternUnion> {
// Properteis

  List<Widget> pages = [
    ChooseAccount(),
    ConvertRate(),
    ReviewAmount(),
  ];
  StepperController stepCtrl = Get.put(StepperController());

// Init State

  @override
  void initState() {
    super.initState();
    stepCtrl.pages = pages;
  }

// Build

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppbar(
        title: 'Western Union',
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            width: MediaQuery.of(context).size.width,
            child: Obx(
              () => Center(
                child: IconStepper(
                  activeStep: stepCtrl.stepIndex.value,
                  activeStepColor: AppColors.secondary,
                  activeStepBorderColor: AppColors.secondary,
                  lineColor: AppColors.secondary,
                  stepColor: AppColors.primary,
                  activeStepBorderWidth: 2,
                  nextButtonIcon: Icon(
                    Ionicons.chevron_forward,
                    color: AppColors.primary,
                    size: 15,
                  ),
                  previousButtonIcon: Icon(
                    Ionicons.chevron_back,
                    color: AppColors.primary,
                    size: 15,
                  ),
                  onStepReached: (index) {
                    stepCtrl.stepIndex.value = index;
                    print(stepCtrl.stepIndex.value);
                  },
                  icons: [
                    Icon(
                      Ionicons.document,
                      color: Colors.white,
                    ),
                    Icon(
                      Ionicons.lock_closed,
                      color: Colors.white,
                    ),
                    Icon(
                      Ionicons.image,
                      color: Colors.white,
                    ),
                  ],
                ),
              ),
            ),
          ),
          Obx(
            () => Column(
              children: [
                stepCtrl.getActivePage(),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
