import 'package:flutter/material.dart';
import 'package:tib_bank/views/widgets/account_card.dart';

class ChooseAccount extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10),
      child: Column(
        children: [
          AccountCard(
            accoutNo: '1234 **** **** 3443',
            address: 'Al Nahda Branch, Dubai',
            amount: '130,000 IQD',
            type: 'Saving Account',
            expiry: '06/23',
            onTap: () {},
          ),
          AccountCard(
            accoutNo: '1234 **** **** 3213',
            address: 'Al Nahda Branch, Dubai',
            amount: '220,000 IQD',
            type: 'Current Account',
            expiry: '03/23',
            onTap: () {},
          ),
        ],
      ),
    );
  }
}
