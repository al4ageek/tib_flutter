import 'package:flutter/material.dart';
import 'package:ionicons/ionicons.dart';
import 'package:tib_bank/config/constants/colors.dart';
import 'package:tib_bank/config/helper/text_styles.dart';
import 'package:tib_bank/config/lang/app_locale.dart';

class SlideThree extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: EdgeInsets.symmetric(vertical: 25),
          child: Center(
            child: Text(
              AppLocalizations.of(context)
                  .translate('upload_documents')
                  .toUpperCase(),
              style: AppText.contentBold(),
              textAlign: TextAlign.center,
            ),
          ),
        ),
        GridView.builder(
          addRepaintBoundaries: true,
          itemCount: 8,
          shrinkWrap: true,
          padding: EdgeInsets.fromLTRB(0, 25, 0, 200),
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2,
            mainAxisSpacing: 10,
            crossAxisSpacing: 10,
            childAspectRatio: 1,
          ),
          itemBuilder: (BuildContext context, index) {
            return Container(
              decoration: BoxDecoration(
                color: AppColors.lightGreen.withAlpha(50),
                border: Border.all(
                  color: AppColors.secondary,
                  style: BorderStyle.solid,
                ),
                borderRadius: BorderRadius.circular(5),
              ),
              child: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Icon(
                      Ionicons.cloud_upload_outline,
                      color: AppColors.secondary,
                      size: 40,
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(
                        vertical: 10,
                      ),
                    ),
                    Text(
                      AppLocalizations.of(context).translate('click_to_upload'),
                      style: AppText.content(
                        color: AppColors.secondary,
                      ),
                    )
                  ],
                ),
              ),
            );
          },
        ),
      ],
    );
  }
}
