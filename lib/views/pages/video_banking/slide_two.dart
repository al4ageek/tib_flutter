import 'package:flutter/material.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:tib_bank/config/constants/colors.dart';
import 'package:tib_bank/config/helper/text_styles.dart';
import 'package:tib_bank/config/lang/app_locale.dart';

class SlideTwo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Container(
            width: 220,
            margin: EdgeInsets.symmetric(vertical: 25),
            child: Image.asset(
              'assets/img/sms.png',
            ),
          ),
          Text(
            AppLocalizations.of(context)
                .translate('enter_otp_sent')
                .toUpperCase(),
            style: AppText.contentBold(),
          ),
          Padding(
            padding: EdgeInsets.only(top: 15),
          ),
          Text(
            '+971 00242133',
            style: AppText.contentBold(color: AppColors.secondary),
          ),
          Padding(
            padding: EdgeInsets.only(top: 40),
          ),
          Form(
            child: Padding(
              padding:
                  const EdgeInsets.symmetric(vertical: 8.0, horizontal: 30),
              child: PinCodeTextField(
                appContext: context,
                pastedTextStyle: AppText.content(),
                length: 4,
                obscureText: true,
                blinkWhenObscuring: true,
                animationType: AnimationType.fade,
                pinTheme: PinTheme(
                  shape: PinCodeFieldShape.box,
                  borderRadius: BorderRadius.circular(8),
                  fieldHeight: 70,
                  fieldWidth: 70,
                  disabledColor: AppColors.lightGreen,
                  activeColor: AppColors.primary,
                  selectedColor: AppColors.primary,
                  inactiveColor: AppColors.lightGreen,
                ),
                cursorColor: AppColors.primary,
                animationDuration: Duration(milliseconds: 300),
                backgroundColor: Colors.transparent,
                enableActiveFill: false,
                keyboardType: TextInputType.number,

                onCompleted: (v) {
                  print("Completed");
                },
                // onTap: () {
                //   print("Pressed");
                // },
                onChanged: (value) {
                  print(value);
                },
                beforeTextPaste: (text) {
                  print("Allowing to paste $text");
                  //if you return true then it will show the paste confirmation dialog. Otherwise if false, then nothing will happen.
                  //but you can show anything you want here, like your pop up saying wrong paste format or etc
                  return true;
                },
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 30, bottom: 10),
            child: Text(
              AppLocalizations.of(context).translate('not_recieve_otp'),
              style: AppText.content(),
            ),
          ),
          InkWell(
            onTap: () {},
            child: Text(
              AppLocalizations.of(context).translate('resend'),
              style: AppText.contentBold(color: AppColors.secondary),
            ),
          ),
        ],
      ),
    );
  }
}
