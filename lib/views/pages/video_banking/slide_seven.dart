import 'package:flutter/material.dart';
import 'package:ionicons/ionicons.dart';
import 'package:tib_bank/config/helper/text_styles.dart';
import 'package:tib_bank/config/lang/app_locale.dart';
import 'package:tib_bank/views/widgets/custom_heading.dart';

class SlideSeven extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: EdgeInsets.symmetric(vertical: 25),
          child: Center(
            child: Text(
              AppLocalizations.of(context)
                  .translate('complete_profile')
                  .toUpperCase(),
              style: AppText.contentBold(),
              textAlign: TextAlign.center,
            ),
          ),
        ),
        Form(
          child: Column(
            children: [
              ExpansionTile(
                title: CustomHeading(
                  title: 'Account Details',
                ),
                children: [
                  Container(
                    padding: EdgeInsets.symmetric(vertical: 15),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: EdgeInsets.only(bottom: 30),
                        ),
                        Text(
                          AppLocalizations.of(context)
                              .translate('account_type')
                              .toUpperCase(),
                          style: AppText.labelText(),
                        ),
                        DropdownButton(
                          isExpanded: true,
                          icon: Icon(
                            Ionicons.chevron_down,
                            size: 18,
                          ),
                          value: 0,
                          hint: Text(
                            'Select Account Type',
                            style: AppText.content(),
                          ),
                          items: [
                            DropdownMenuItem(
                              child: Text(
                                'Person',
                                style: AppText.content(),
                              ),
                              value: 0,
                            ),
                            DropdownMenuItem(
                              child: Text(
                                'Company',
                                style: AppText.content(),
                              ),
                              value: 1,
                            ),
                          ],
                          onChanged: (e) {},
                        ),
                        Padding(
                          padding: EdgeInsets.only(bottom: 30),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              ExpansionTile(
                title: CustomHeading(
                  title: 'Customer Information',
                ),
              ),
              ExpansionTile(
                title: CustomHeading(
                  title: 'Address & Contact',
                ),
              ),
              ExpansionTile(
                title: CustomHeading(
                  title: 'Financial Matters',
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
