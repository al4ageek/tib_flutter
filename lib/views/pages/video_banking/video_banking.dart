import 'package:flutter/material.dart';
import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:get/instance_manager.dart';
import 'package:ionicons/ionicons.dart';
import 'package:tib_bank/config/constants/colors.dart';
import 'package:tib_bank/config/lang/app_locale.dart';
import 'package:tib_bank/controller/stepper_ctrl.dart';
import 'package:tib_bank/views/pages/video_banking/slide_five.dart';
import 'package:tib_bank/views/pages/video_banking/slide_four.dart';
import 'package:tib_bank/views/pages/video_banking/slide_one.dart';
import 'package:tib_bank/views/pages/video_banking/slide_seven.dart';
import 'package:tib_bank/views/pages/video_banking/slide_six.dart';
import 'package:tib_bank/views/pages/video_banking/slide_three.dart';
import 'package:tib_bank/views/pages/video_banking/slide_two.dart';
import 'package:tib_bank/views/pages/video_banking/video_bank_steps.dart';
import 'package:tib_bank/views/widgets/app_bar.dart';
import 'package:tib_bank/views/widgets/app_button.dart';

class VideoBanking extends StatefulWidget {
  @override
  _VideoBankingState createState() => _VideoBankingState();
}

class _VideoBankingState extends State<VideoBanking> {
// Properites

  List<Widget> pages = [
    SlideOne(),
    SlideTwo(),
    SlideThree(),
    SlideFour(),
    SlideFive(),
    SlideSix(),
    SlideSeven(),
  ];
  StepperController stepCtrl = Get.put(StepperController());

// Initstate

  @override
  void initState() {
    super.initState();
    stepCtrl.pages = pages;
  }

// Build

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.primary,
      appBar: CustomAppbar(
        title: AppLocalizations.of(context).translate('create_profile'),
        bgColor: Colors.transparent,
        elevation: 0,
        textColor: AppColors.white,
        centerTitle: false,
        iconColor: AppColors.white,
        brightness: Brightness.dark,
      ),
      body: Stack(
        children: [
          Positioned(
            left: 15,
            top: 12,
            child: Container(
              width: MediaQuery.of(context).size.width - 30,
              height: MediaQuery.of(context).size.height,
              decoration: BoxDecoration(
                color: Colors.white.withAlpha(80),
                borderRadius: BorderRadius.circular(10),
              ),
            ),
          ),
          Positioned(
            left: 30,
            top: 0,
            child: Container(
              width: MediaQuery.of(context).size.width - 60,
              height: MediaQuery.of(context).size.height - 140,
              decoration: BoxDecoration(
                color: Colors.white.withAlpha(80),
                borderRadius: BorderRadius.circular(10),
              ),
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(10),
            ),
            child: Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              padding: EdgeInsets.all(15),
              child: SingleChildScrollView(
                child: VideoBankSteps(),
              ),
            ),
            margin: EdgeInsets.only(top: 25),
          ),
          Positioned(
            bottom: 0,
            left: 0,
            child: SafeArea(
              child: Container(
                width: MediaQuery.of(context).size.width,
                padding: EdgeInsets.symmetric(horizontal: 15),
                child: CustomButton(
                  title: AppLocalizations.of(context).translate('next'),
                  // icon: Ionicons.arrow_forward,
                  backgroundColor: AppColors.primary,
                  textColor: Colors.white,
                  radius: 5,
                  icon: Ionicons.arrow_forward,
                  onTap: () {
                    stepCtrl.nextPage();
                  },
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
