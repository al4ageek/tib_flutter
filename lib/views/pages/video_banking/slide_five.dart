import 'package:flutter/material.dart';
import 'package:ionicons/ionicons.dart';
import 'package:tib_bank/config/constants/colors.dart';
import 'package:tib_bank/config/helper/text_styles.dart';
import 'package:tib_bank/config/lang/app_locale.dart';

class SlideFive extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: EdgeInsets.symmetric(vertical: 25),
          child: Center(
            child: Text(
              AppLocalizations.of(context)
                  .translate('digital_signature')
                  .toUpperCase(),
              style: AppText.contentBold(),
              textAlign: TextAlign.center,
            ),
          ),
        ),
        Container(
          margin: EdgeInsets.only(top: 15),
          height: 400,
          decoration: BoxDecoration(
            color: AppColors.lightGreen.withAlpha(50),
            border: Border.all(
              color: AppColors.secondary,
              style: BorderStyle.solid,
            ),
            borderRadius: BorderRadius.circular(5),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              IconButton(
                icon: Icon(Ionicons.arrow_back),
                onPressed: () {},
              ),
              IconButton(
                icon: Icon(Ionicons.arrow_forward),
                onPressed: () {},
              )
            ],
          ),
        ),
      ],
    );
  }
}
