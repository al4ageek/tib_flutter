import 'package:country_code_picker/country_code_picker.dart';
import 'package:flutter/material.dart';
import 'package:ionicons/ionicons.dart';
import 'package:tib_bank/config/constants/colors.dart';
import 'package:tib_bank/config/helper/text_styles.dart';
import 'package:tib_bank/config/helper/ui_styles.dart';
import 'package:tib_bank/config/lang/app_locale.dart';

class SlideOne extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    int accountType;
    return Container(
      padding: EdgeInsets.all(25),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Center(
            child: Text(
              AppLocalizations.of(context)
                  .translate('enter_info')
                  .toUpperCase(),
              style: AppText.content(),
              textAlign: TextAlign.center,
            ),
          ),
          Padding(
            padding: EdgeInsets.only(bottom: 30),
          ),
          Text(
            AppLocalizations.of(context)
                .translate('name_surname')
                .toUpperCase(),
            style: AppText.labelText(),
          ),
          TextFormField(
            decoration: InputDecoration(
              hintText:
                  AppLocalizations.of(context).translate('enter_name_surname'),
              hintStyle: AppText.content(),
            ),
            keyboardType: TextInputType.text,
            textInputAction: TextInputAction.next,
            // validator: Helper.validateEmpty,
            onFieldSubmitted: (term) {},
          ),
          Padding(
            padding: EdgeInsets.only(bottom: 30),
          ),
          Text(
            AppLocalizations.of(context).translate('email').toUpperCase(),
            style: AppText.labelText(),
          ),
          TextFormField(
            decoration: InputDecoration(
              hintText: AppLocalizations.of(context).translate('enter_email'),
              hintStyle: AppText.content(),
            ),
            keyboardType: TextInputType.text,
            textInputAction: TextInputAction.next,
            // validator: Helper.validateEmpty,
            onFieldSubmitted: (term) {},
          ),
          Padding(
            padding: EdgeInsets.only(bottom: 30),
          ),
          Text(
            AppLocalizations.of(context).translate('phone').toUpperCase(),
            style: AppText.labelText(),
          ),
          Row(
            children: [
              Container(
                width: 100,
                child: CountryCodePicker(
                  onChanged: print,
                  initialSelection: 'AE',
                  favorite: [
                    '+971',
                    'AE',
                  ],
                  boxDecoration: UIStyles.cardDecoration(
                    radius: 10,
                  ),
                  showFlagMain: true,
                  showFlag: true,
                  showCountryOnly: false,
                  showOnlyCountryWhenClosed: true,
                  alignLeft: true,
                  hideMainText: false,
                  dialogTextStyle: AppText.content(),
                  closeIcon: Icon(
                    Ionicons.close,
                    color: AppColors.danger,
                    size: 30,
                  ),
                  barrierColor: Colors.black.withAlpha(70),
                  dialogSize: Size.fromHeight(500),
                  searchDecoration: InputDecoration(
                    contentPadding: EdgeInsets.all(5),
                  ),
                ),
                decoration: BoxDecoration(
                    border: Border(
                  bottom: BorderSide(
                    color: AppColors.text,
                  ),
                )),
              ),
              Expanded(
                child: TextFormField(
                  decoration: InputDecoration(
                    hintText:
                        AppLocalizations.of(context).translate('enter_phone'),
                    hintStyle: AppText.content(),
                  ),
                  keyboardType: TextInputType.text,
                  textInputAction: TextInputAction.next,
                  // validator: Helper.validateEmpty,
                  onFieldSubmitted: (term) {},
                ),
              ),
            ],
          ),
          Padding(
            padding: EdgeInsets.only(bottom: 30),
          ),
          Text(
            AppLocalizations.of(context)
                .translate('account_type')
                .toUpperCase(),
            style: AppText.labelText(),
          ),
          DropdownButton(
            isExpanded: true,
            icon: Icon(
              Ionicons.chevron_down,
              size: 18,
            ),
            value: accountType,
            hint: Text(
              'Select Account Type',
              style: AppText.content(),
            ),
            items: [
              DropdownMenuItem(
                child: Text(
                  'Person',
                  style: AppText.content(),
                ),
                value: 0,
              ),
              DropdownMenuItem(
                child: Text(
                  'Company',
                  style: AppText.content(),
                ),
                value: 1,
              ),
            ],
            onChanged: (e) {},
          ),
        ],
      ),
    );
  }
}
