import 'package:flutter/material.dart';
import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:get/instance_manager.dart';
import 'package:im_stepper/stepper.dart';
import 'package:ionicons/ionicons.dart';
import 'package:tib_bank/config/constants/colors.dart';
import 'package:tib_bank/controller/stepper_ctrl.dart';

class VideoBankSteps extends StatelessWidget {
// Properites

  final StepperController stepCtrl = Get.find();

// Build

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          width: MediaQuery.of(context).size.width,
          child: Center(
            child: Obx(
              () => IconStepper(
                activeStep: stepCtrl.stepIndex.value,
                activeStepColor: AppColors.secondary,
                activeStepBorderColor: AppColors.secondary,
                lineColor: AppColors.secondary,
                stepColor: AppColors.primary,
                nextButtonIcon: Icon(
                  Ionicons.chevron_forward,
                  color: AppColors.primary,
                  size: 15,
                ),
                previousButtonIcon: Icon(
                  Ionicons.chevron_back,
                  color: AppColors.primary,
                  size: 15,
                ),
                onStepReached: (index) {
                  stepCtrl.stepIndex.value = index;
                  print(stepCtrl.stepIndex.value);
                },
                icons: [
                  Icon(
                    Ionicons.document,
                    color: Colors.white,
                  ),
                  Icon(
                    Ionicons.lock_closed,
                    color: Colors.white,
                  ),
                  Icon(
                    Ionicons.image,
                    color: Colors.white,
                  ),
                  Icon(
                    Ionicons.person,
                    color: Colors.white,
                  ),
                  Icon(
                    Ionicons.finger_print,
                    color: Colors.white,
                  ),
                  Icon(
                    Ionicons.location,
                    color: Colors.white,
                  ),
                  Icon(
                    Ionicons.list,
                    color: Colors.white,
                  ),
                ],
              ),
            ),
          ),
        ),
        Obx(
          () => Column(
            children: [
              stepCtrl.getActivePage(),
            ],
          ),
        ),
      ],
    );
  }
}
