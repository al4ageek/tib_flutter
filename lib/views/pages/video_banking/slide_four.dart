import 'package:flutter/material.dart';
import 'package:ionicons/ionicons.dart';
import 'package:tib_bank/config/constants/colors.dart';
import 'package:tib_bank/config/helper/text_styles.dart';
import 'package:tib_bank/config/lang/app_locale.dart';

class SlideFour extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: EdgeInsets.symmetric(vertical: 25),
          child: Center(
            child: Text(
              AppLocalizations.of(context)
                  .translate('upload_photo')
                  .toUpperCase(),
              style: AppText.contentBold(),
              textAlign: TextAlign.center,
            ),
          ),
        ),
        Container(
          margin: EdgeInsets.only(top: 15),
          height: 400,
          decoration: BoxDecoration(
            color: AppColors.lightGreen.withAlpha(50),
            border: Border.all(
              color: AppColors.secondary,
              style: BorderStyle.solid,
            ),
            borderRadius: BorderRadius.circular(5),
          ),
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Icon(
                  Ionicons.person_outline,
                  color: AppColors.secondary,
                  size: 70,
                ),
                Padding(
                  padding: EdgeInsets.symmetric(
                    vertical: 10,
                  ),
                ),
                Text(
                  AppLocalizations.of(context).translate('click_to_upload'),
                  style: AppText.content(
                    color: AppColors.secondary,
                  ),
                )
              ],
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.only(
            top: 15,
          ),
        ),
        Text(
          AppLocalizations.of(context).translate('select_from_gallery'),
          style: AppText.content(
            color: AppColors.secondary,
          ),
        )
      ],
    );
  }
}
