import 'package:get/instance_manager.dart';
import 'package:tib_bank/config/lang/app_language.dart';
import 'package:tib_bank/controller/app_ctrl.dart';
import 'package:tib_bank/controller/home_ctrl.dart';

class HomeBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => AppController());
    Get.lazyPut(() => AppLanguage());
    Get.lazyPut(() => HomeController());
  }
}
