import 'package:get/instance_manager.dart';
import 'package:tib_bank/controller/dashboard_ctrl.dart';

class DashboardBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => DashboardController());
  }
}
