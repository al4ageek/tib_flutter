import 'package:get/instance_manager.dart';
import 'package:tib_bank/controller/video_banking_ctrl.dart';

class VideoBankingBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => VideoBankingController());
  }
}
