import 'package:get/instance_manager.dart';
import 'package:tib_bank/controller/login_ctrl.dart';

class LoginBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => LoginController());
  }
}
