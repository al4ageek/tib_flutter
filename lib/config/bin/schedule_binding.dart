import 'package:get/instance_manager.dart';
import 'package:tib_bank/controller/schedule_ctrl.dart';

class SchedulePageBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => ScheduleCallController());
  }
}
