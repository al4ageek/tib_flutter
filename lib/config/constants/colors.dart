import 'package:flutter/material.dart';

class AppColors {
  static const Color primary = Color(0xFF1D6F42);
  static const Color secondary = Color(0xFFD1AF5A);
  static const Color text = Color(0xFF214134);
  static const Color accent = Color(0xFF098B5C);
  static const Color success = Color(0xFF098B5C);
  static const Color warning = Color(0xFFE9A405);
  static const Color danger = Color(0xFFDE2D42);
  static const Color white = Color(0xFFFFFFFF);
  static const Color background = Color(0xFFF7F7F7);
  static const Color lightGreen = Color(0xFFC8E5DA);
}
