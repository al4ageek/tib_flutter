abstract class Routes {
  static const HOME = '/home';
  static const DASHBOARD = '/dashboard';
  static const LOGIN = '/login';
  static const VIDEO_BANKING = '/video_banking';
  static const SCHEDULE_BANKING = '/schedule_banking';
}
