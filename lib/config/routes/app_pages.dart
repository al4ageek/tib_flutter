import 'package:get/get.dart';
import 'package:tib_bank/config/bin/dashboard_binding.dart';
import 'package:tib_bank/config/bin/home_binding.dart';
import 'package:tib_bank/config/bin/login_binding.dart';
import 'package:tib_bank/config/bin/schedule_binding.dart';
import 'package:tib_bank/config/bin/video_banking_binding.dart';
import 'package:tib_bank/views/pages/dashboard/dashboard.dart';
import 'package:tib_bank/views/pages/home/home.dart';
import 'package:tib_bank/views/pages/login/login.dart';
import 'package:tib_bank/views/pages/schedule_call/schedule_call.dart';
import 'package:tib_bank/views/pages/video_banking/video_banking.dart';
import 'app_routes.dart';

class AppPages {
  static const INITIAL = Routes.LOGIN;

  static final appPages = [
    GetPage(
      name: Routes.LOGIN,
      page: () => Login(),
      binding: LoginBinding(),
    ),
    GetPage(
      name: Routes.VIDEO_BANKING,
      page: () => VideoBanking(),
      binding: VideoBankingBinding(),
    ),
    GetPage(
      name: Routes.SCHEDULE_BANKING,
      page: () => ScheduleCall(),
      binding: SchedulePageBinding(),
    ),
    GetPage(
      name: Routes.HOME,
      page: () => Home(),
      binding: HomeBinding(),
    ),
    GetPage(
      name: Routes.HOME,
      page: () => Dashboard(),
      binding: DashboardBinding(),
    ),
  ];
}
