import 'package:flutter/material.dart';
import 'package:tib_bank/config/constants/colors.dart';

class UIStyles {
  //  Card Styling

  static BoxDecoration cardDecoration({double radius = 0}) {
    return BoxDecoration(
      color: Colors.white,
      borderRadius: BorderRadius.circular(radius),
      border: Border.all(
        color: AppColors.text.withAlpha(20),
      ),
      boxShadow: [
        BoxShadow(
          color: AppColors.text.withAlpha(30),
          blurRadius: 3,
          offset: Offset(0, 0),
        ),
      ],
    );
  }

  //  Card Styling

  static BoxDecoration borderCard(Color color) {
    return BoxDecoration(
      color: Colors.white,
      borderRadius: BorderRadius.circular(5),
      boxShadow: [
        BoxShadow(
          color: AppColors.text.withAlpha(50),
          blurRadius: 2,
          offset: Offset(0, 0),
        ),
      ],
      border: Border(
        left: BorderSide(
          color: color,
          width: 3,
          style: BorderStyle.none,
        ),
      ),
    );
  }
}
