import 'package:flutter/material.dart';
import 'package:tib_bank/config/constants/colors.dart';

class AppText {
  // Heading

  static TextStyle primaryHeading({Color color = AppColors.text}) {
    return TextStyle(
      color: color,
      fontSize: 30,
      fontWeight: FontWeight.bold,
      fontFamily: 'Cairo',
    );
  }

  static TextStyle secondaryHeading({Color color = AppColors.text}) {
    return TextStyle(
      color: color,
      fontSize: 20,
      fontWeight: FontWeight.bold,
      fontFamily: 'Cairo',
      height: 1.4,
    );
  }

  // body Text

  static TextStyle content({
    Color color = AppColors.text,
    double height = 1.5,
  }) {
    return TextStyle(
      color: color != null ? color : AppColors.text,
      fontSize: 17,
      fontWeight: FontWeight.normal,
      letterSpacing: 0.03,
      height: height != null ? height : null,
      fontFamily: 'Cairo',
    );
  }

  // Underline TExt

  static TextStyle contentUnderline({
    Color color = AppColors.text,
    double height = 1.5,
  }) {
    return TextStyle(
      color: color != null ? color : AppColors.text,
      fontSize: 14,
      fontWeight: FontWeight.bold,
      letterSpacing: 0.03,
      decoration: TextDecoration.underline,
      height: height != null ? height : null,
      fontFamily: 'Cairo',
    );
  }

  // Small Text

  static TextStyle contentBold({Color color = AppColors.text}) {
    return TextStyle(
      color: color,
      fontSize: 18,
      fontWeight: FontWeight.bold,
      fontFamily: 'Cairo',
    );
  }

  //  Button Text

  static TextStyle buttonText({Color color = AppColors.text}) {
    return TextStyle(
      color: color,
      fontSize: 18,
      letterSpacing: 0.03,
      fontFamily: 'Cairo',
    );
  }

  //  Menu Items Text

  static TextStyle menuText({Color color = AppColors.text}) {
    return TextStyle(
      color: color,
      fontSize: 13,
      fontFamily: 'Cairo',
    );
  }

//  body Label Titles

  static TextStyle labelText({
    Color color = AppColors.secondary,
  }) {
    return TextStyle(
      color: color,
      fontSize: 14,
      letterSpacing: 0.03,
      fontFamily: 'Cairo',
      fontWeight: FontWeight.bold,
    );
  }

  // Small Text Bold

  static TextStyle smallTextBold({Color color = AppColors.text}) {
    return TextStyle(
      color: color,
      fontSize: 12,
      fontWeight: FontWeight.bold,
      fontFamily: 'Cairo',
    );
  }

  //  Tabs Text

  static TextStyle tabsText({Color color = AppColors.text}) {
    return TextStyle(
      color: color,
      fontSize: 18,
      fontFamily: 'Cairo',
    );
  }
}
