import 'package:flutter/material.dart';
import 'package:flutter_styled_toast/flutter_styled_toast.dart';
import 'package:get/get.dart';
import 'package:google_map_location_picker/google_map_location_picker.dart';
import 'package:intl/intl.dart';
import 'package:ionicons/ionicons.dart';
import 'package:tib_bank/config/constants/colors.dart';
import 'package:tib_bank/config/helper/text_styles.dart';
import 'package:tib_bank/config/helper/ui_styles.dart';
import 'package:tib_bank/config/lang/app_language.dart';
import 'package:tib_bank/views/widgets/app_button.dart';

class Helper {
// Navigation

  // 1. Redirect Page

  static to(Widget page, {bool isModal = false}) {
    Get.to(page, fullscreenDialog: isModal);
  }

// 2. Redirect using flutter default

  static toRoute(
    BuildContext context,
    Widget page,
  ) {
    // Get.to(page);
    Navigator.of(context).pushReplacement(
      MaterialPageRoute(
        builder: (_) => page,
      ),
    );
  }

  // 3. Replace Page

  static offAll(Widget page) {
    Get.offAll(page);
  }

// Date Picker

  static Future<String> selectDate(BuildContext context) async {
    var date = await showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(2019, 8),
      lastDate: DateTime.now(),
    );
    return date.toString();
  }

// Format Date

  static String formatDate(DateTime unFormatDate) {
    String formattedDate = DateFormat('yyyy-MM-dd').format(unFormatDate);
    return formattedDate;
  }

// Show Toast

  static showMessage(String message, [bool success]) {
    showToast(
      message,
      axis: Axis.horizontal,
      alignment: Alignment.center,
      position: StyledToastPosition.top,
      toastHorizontalMargin: 25,
      fullWidth: true,
      duration: Duration(seconds: 3),
      backgroundColor:
          success != null && success ? AppColors.success : Colors.red,
      animation: StyledToastAnimation.slideFromTop,
      animDuration: Duration(
        milliseconds: 100,
      ),
    );
  }

// Check Direction

  static bool isRtl(BuildContext context) {
    AppLanguage appLanguage = Get.find();
    if (appLanguage.appLocal.languageCode == 'ar') {
      return true;
    } else {
      return false;
    }
  }

// Validations

  static String validateEmpty(String value) {
    if (value.length == 0)
      return 'This field cannot be empty';
    else
      return null;
  }

  static String validateEmail(String value) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
    if (!regex.hasMatch(value))
      return 'Enter Valid Email';
    else
      return null;
  }

// Field Focus Change

  static fieldFocusChange(
    BuildContext context,
    FocusNode currentFocus,
    FocusNode nextFocus,
  ) {
    currentFocus.unfocus();
    FocusScope.of(context).requestFocus(nextFocus);
  }

// Show Bottom Sheet

  static showSheet(BuildContext context, String title, Widget content) {
    showModalBottomSheet(
      context: context,
      backgroundColor: AppColors.white,
      elevation: 0,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15),
      ),
      builder: (BuildContext context) {
        return Container(
          height: 400,
          child: Column(
            children: [
              Container(
                width: 50,
                height: 5,
                margin: EdgeInsets.only(top: 10, bottom: 20),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(50),
                  color: AppColors.secondary,
                ),
              ),
              Center(
                child: Text(
                  title,
                  style: AppText.secondaryHeading(),
                ),
              ),
              content,
            ],
          ),
        );
      },
    );
  }

// Show Modal

  static showModal({
    @required BuildContext context,
    String title = 'Are you sure',
    Function onAction,
    String message = 'Please confirm your want to proceed!',
    IconData icon = Ionicons.information,
    Color color = AppColors.primary,
  }) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return Container(
          height: 300,
          child: AlertDialog(
            backgroundColor: Colors.transparent,
            elevation: 0,
            content: SingleChildScrollView(
              child: Container(
                decoration: UIStyles.cardDecoration(
                  radius: 20,
                ),
                child: Stack(
                  overflow: Overflow.visible,
                  children: [
                    Positioned(
                      top: -80,
                      left: 110,
                      child: Container(
                        width: 80,
                        height: 80,
                        margin: EdgeInsets.symmetric(vertical: 40),
                        decoration: BoxDecoration(
                          color: color,
                          shape: BoxShape.circle,
                          border: Border.all(
                            color: AppColors.white,
                            width: 3,
                          ),
                        ),
                        child: Center(
                          child: Icon(
                            icon,
                            color: AppColors.white,
                          ),
                        ),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.fromLTRB(25, 80, 25, 25),
                      child: Column(
                        children: [
                          Text(
                            title,
                            style: AppText.secondaryHeading(),
                          ),
                          Container(
                            padding: EdgeInsets.all(15),
                            child: Text(
                              message,
                              style: AppText.content(),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(top: 20),
                          ),
                          CustomButton(
                            onTap: () {
                              onAction();
                            },
                            backgroundColor: color,
                            borderColor: color,
                            hasBorder: false,
                            textColor: AppColors.white,
                            radius: 5,
                            title: 'Proceed',
                            icon: Ionicons.arrow_forward,
                          ),
                          Padding(
                            padding: EdgeInsets.only(top: 10),
                          ),
                          CustomButton(
                            onTap: () {
                              Navigator.pop(context);
                            },
                            backgroundColor: AppColors.lightGreen,
                            borderColor: AppColors.lightGreen,
                            hasBorder: false,
                            textColor: AppColors.text,
                            radius: 5,
                            icon: Ionicons.trash_bin_outline,
                            title: 'Cancel',
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
