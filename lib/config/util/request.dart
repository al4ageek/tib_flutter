import 'dart:async';
import 'package:http/http.dart' as http;

class Request {
  final String url;
  final dynamic body;
  final String token;

  Request(this.url, this.body, [this.token]);

  Future<http.Response> post() async {
    var response = await http.post(
      url,
      body: body,
      headers: {
        "Accept": "application/json",
        "Content-Type": "application/json",
      },
    );
    print('Response status: ${response.statusCode}');
    print('Response body: ${response.body}');
    return response;
  }
}
