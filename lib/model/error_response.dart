class ErrorResponse {
  String reason;
  String reasonCode;

  ErrorResponse({this.reason, this.reasonCode});

  ErrorResponse.fromJson(Map<String, dynamic> json) {
    reason = json['reason'];
    reasonCode = json['reasonCode'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['reason'] = this.reason;
    data['reasonCode'] = this.reasonCode;
    return data;
  }
}
