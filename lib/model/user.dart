class User {
  int id;
  String cif;
  String userId;
  Null email;
  String firstName;
  String middleName;
  String lastName;

  User(
      {this.id,
      this.cif,
      this.userId,
      this.email,
      this.firstName,
      this.middleName,
      this.lastName});

  User.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    cif = json['cif'];
    userId = json['userId'];
    email = json['email'];
    firstName = json['firstName'];
    middleName = json['middleName'];
    lastName = json['lastName'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['cif'] = this.cif;
    data['userId'] = this.userId;
    data['email'] = this.email;
    data['firstName'] = this.firstName;
    data['middleName'] = this.middleName;
    data['lastName'] = this.lastName;
    return data;
  }
}
