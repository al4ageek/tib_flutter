import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:tib_bank/config/constants/strings.dart';
import 'package:tib_bank/config/helper/helper.dart';
import 'package:tib_bank/config/helper/helper.dart';
import 'package:tib_bank/config/util/request.dart';
import 'package:tib_bank/controller/app_ctrl.dart';
import 'package:tib_bank/views/pages/home/home.dart';
import 'package:tib_bank/model/error_response.dart';
import 'package:tib_bank/model/user.dart';



class LoginController extends GetxController {
  var isLoading = false.obs;
  var validate = false.obs;
  String username, password;
  GlobalKey<FormState> key = new GlobalKey();
  var pwdVisible = false.obs;
  final AppController appCtrl = Get.find();
// On Init

  @override
  void onInit() {
    super.onInit();
    nameCtrl.addListener(saveText);
    passCtrl.addListener(saveText);
  }

// Set Text

  saveText() {
    username = nameCtrl.text;
    password = passCtrl.text;
  }

// On Dispose

  @override
  void onClose() {
    nameCtrl.dispose();
    passCtrl.dispose();
    super.onClose();
  }

  // Text Controllers & Focus nodes

  final nameCtrl = TextEditingController();
  final passCtrl = TextEditingController();
  final FocusNode nameFocus = FocusNode();
  final FocusNode passFocus = FocusNode();

// Do Login

  Future doLogin() async {
    if (key.currentState.validate()) {
      isLoading.value = true;
      key.currentState.save();
      String data = '{"UserName": "$username", "Password": "$password"}';
      print(data);
      Request request = Request(
        AppStrings.baseURL + AppStrings.login,
        data,
      );
      request.post().then((response) {
        isLoading.value = false;
        if (response.statusCode == 200) {
          print(response.body);
          nameCtrl.clear();
          passCtrl.clear();
          print(response.body);
          var responseData = json.decode(response.body);
          print(responseData);
          User user = User.fromJson(responseData['customer']);
           appCtrl.saveUser(user);
          Helper.offAll(Home());         
        }else if (response.statusCode == 400) {

            isLoading.value = false;
              var responseData = json.decode(response.body);
            ErrorResponse error = ErrorResponse.fromJson(responseData);

          Helper.showMessage(
            error.reason,
          );
        }
         else {          
          isLoading.value = false;
          Helper.showMessage(
            'Some Error Occured',
          );
        }
      });
    }
  }
}
