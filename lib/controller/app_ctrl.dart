import 'dart:convert';

import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:get/instance_manager.dart';
import 'package:tib_bank/config/lang/app_language.dart';
import 'package:tib_bank/config/util/shared_pref_manager.dart';
import 'package:tib_bank/model/user.dart';

class AppController extends GetxController {
// Properteis

  AppLanguage appLanguage = Get.find();

  setLocale() async {
    await appLanguage.fetchLocale();
    print(appLanguage.appLocal);
  }

// Save User

  Future saveUser(User user) async {
    SharedPrefManager.setString(
      SharedPrefManager.user,
      json.encode(
          user.toJson(),
          ),
    );
  }

// Get User

  Future<User> getUser() async {
    User newUser;
    await SharedPrefManager.getString(SharedPrefManager.user).then((userJson) {
      Map userMap = jsonDecode(userJson);
      User user = User.fromJson(userMap);
      newUser = user;     
    });
    if (newUser != null) {
      return newUser;
    } else {
      return null;
    }
  }
}
