import 'package:flutter/material.dart';
import 'package:get/get.dart';

class StepperController extends GetxController {
// Properites

  var stepIndex = 0.obs;
  List<Widget> pages = [];

// Next Page

  nextPage() {
    if (stepIndex.value < pages.length) {
      stepIndex.value++;
    }
    print(stepIndex.value);
  }

// Prev Page

  prevPage() {
    if (stepIndex.value != 0) {
      stepIndex.value--;
    }
  }

// get Active Step

  getActivePage() {
    return pages[stepIndex.value];
  }
}
