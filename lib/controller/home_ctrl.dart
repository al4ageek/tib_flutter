import 'dart:developer';
import 'package:flutter/material.dart';
import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:tib_bank/views/pages/dashboard/dashboard.dart';
import 'package:tib_bank/views/pages/more/more.dart';
import 'package:tib_bank/views/pages/my_accounts/my_accounts.dart';
import 'package:tib_bank/views/pages/services/services.dart';
import 'package:get/get.dart';
import 'package:tib_bank/controller/app_ctrl.dart';
import 'package:tib_bank/model/user.dart';

class HomeController extends GetxController {
// Properties
  AppController appCtrl = Get.find();
  var currentPage = 0.obs;
  User user;
  var screens = [
    Dashboard(),
    MyAccounts(),
    Services(),
    More(),
  ];
  final PageStorageBucket bucket = PageStorageBucket();
  Widget currentScreen;

// On Init

  @override
  void onInit() {
    super.onInit();
    currentScreen = Dashboard();
    appCtrl.getUser().then((value) => user = value);
  }


  setActivePage(Widget page, int index) {
    currentPage.value = index;
    currentScreen = page;
    print('active index ${currentPage.value}');
  }
}
