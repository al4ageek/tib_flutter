import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:tib_bank/views/widgets/deal_slide.dart';

class DashboardController extends GetxController {
// Properties

  List<Widget> slides = [DealSlide()];
  int tabs = 5;
  var tabIndex = 0.obs;
}
