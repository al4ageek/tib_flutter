import 'package:flutter/material.dart';
import 'package:flutter_styled_toast/flutter_styled_toast.dart';
import 'package:get/get.dart';
import 'package:tib_bank/config/routes/app_pages.dart';
import 'config/constants/colors.dart';
import 'config/lang/app_language.dart';
import 'config/lang/app_locale.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'controller/app_ctrl.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  AppLanguage appLanguage = AppLanguage();
  await appLanguage.fetchLocale();
  runApp(MyApp(
    appLanguage: appLanguage,
  ));
}

class MyApp extends StatelessWidget {
// Properties

  final AppLanguage appLanguage;
  final AppLanguage lang = Get.put(AppLanguage(), permanent: true);
  final AppController appCtrl = Get.put(AppController(), permanent: true);

// Constructor

  MyApp({this.appLanguage});

// Build

  @override
  Widget build(BuildContext context) {
    return StyledToast(
      locale: lang.appLocal,
      child: GetMaterialApp(
        title: 'Al Taif Islamic Bank',
        locale: lang.appLocal,
        fallbackLocale: lang.appLocal,
        localizationsDelegates: [
          AppLocalizations.delegate,
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
        ],
        supportedLocales: [
          Locale('en', 'US'),
          Locale('ar', ''),
        ],
        initialRoute: AppPages.INITIAL,
        theme: ThemeData(
          dividerColor: Colors.transparent,
          dividerTheme: DividerThemeData(
            color: Colors.grey[200],
          ),
          primaryColor: AppColors.primary,
          brightness: Brightness.light,
          backgroundColor: AppColors.white,
          accentColor: AppColors.primary,
          visualDensity: VisualDensity.adaptivePlatformDensity,
          fontFamily: 'Roboto',
          canvasColor: AppColors.white,
          buttonColor: AppColors.primary,
          cardColor: Colors.white,
          iconTheme: IconThemeData(
            color: AppColors.text,
            size: 25,
          ),
          appBarTheme: AppBarTheme(
            centerTitle: false,
            brightness: Brightness.light,
            iconTheme: IconThemeData(
              color: AppColors.text,
              size: 22,
            ),
            actionsIconTheme: IconThemeData(
              color: AppColors.text,
              size: 22,
            ),
            color: Colors.white,
            elevation: 0,
          ),
        ),
        getPages: AppPages.appPages,
      ),
    );
  }
}
